import numpy as np

from diamond_geoproc.background_generation import BgType, PolarMeshData,\
    ReliefDataNotAvailableException, SurfaceTypeDataNotAvailableException
from diamond_geoproc.misc import distances_to_sealevel
from diamond_geoproc.slope_angle_calculations import calculate_slope_angle_mesh
from diamond_geoproc.surface_rcs import DafeSurfaceType, LandReflType, surface_rcs_per_unit


class EarthPolarMap:
    EARTH_RADIUS = 6371000.  # Mean Earth radius in m
    EFF_EARTH_RADIUS = 1.33 * EARTH_RADIUS  # Effective Earth radius in m
    # FIXME: поставить 4/3 Rз, это имеет значение

    def __init__(self, lon, lat, terrain, surface):
        self._lon_stand = float(lon)
        self._lat_stand = float(lat)
        self.terrain = terrain
        self.surface = surface

    def _calc_els_and_vis(self, mesh_data, slopeangs_mesh_data, h_raw_mesh,
                          h_raw_mesh_for_slopeangs, h_above_sea,
                          sealevel_distances, sealevel_distances_for_slopeangs,
                          compatibility_mode=True):
        """
        Расчёт карты углов падения луча на фацеты земли, карты видимости
        участков земли и карты углов закрытия.

        Args:
            mesh_data(diamond_geoproc.terrain_map_api.olarMeshData): данные о
              сетке (дальности, азимуты и др.)
            slopeangs_mesh_data(numpy.ndarray(dtype=np.float64)): углы падения
              луча на фацеты
            h_raw_mesh(numpy.ndarray(dtype=np.float64)):
            h_raw_mesh_for_slopeangs:
            h_above_sea:
            sealevel_distances:
            sealevel_distances_for_slopeangs:
            compatibility_mode: считать углы падения так же, как они считались
              в старом XRadar (bug-to-bug-совместимость!)
        Returns:
            polar_maps: словарь с полярными картами
                elevations: угломестные координаты центров фацетов земной
                    поверхности относительно РЛС
                running_max_elevations: скользящие максимумы угломестных
                координат фацетов (т.е. карта углов закрытия)
                earth_visibility: карта видимости фацетов
                h_mesh: расстояния от центров фацетов относительно плоскости,
                    касательной к земному эллипсоиду в точке стояния РЛС
                slope_angs: углы падения луча (в радианах) на центр каждого
                    фацета
            mesh_data: расчетная сетка координат полярных карт
            h_above_sea: высота над уровнем моря

        """

        # Углы между линиями "центр Земли - точка стояния" и
        # "центр Земли - центр фацета" для каждого фацета
        angs_from_vert = np.arctan(mesh_data.ranges / self.EFF_EARTH_RADIUS)
        # Высоты центров фацетов относительно плоскости, касательной к Земле
        # в точке стояния
        h_mesh = h_raw_mesh * np.cos(angs_from_vert[None, :]) + sealevel_distances

        angs_from_vert_for_slopeangs = np.arctan(
            slopeangs_mesh_data.ranges / self.EFF_EARTH_RADIUS)
        h_mesh_for_slopeangs = h_raw_mesh_for_slopeangs * np.cos(
            angs_from_vert_for_slopeangs[None, :]
        ) + sealevel_distances_for_slopeangs

        elevations = np.arctan((h_mesh - h_above_sea) / mesh_data.range_mesh)

        running_max_elevations = np.maximum.accumulate(elevations, axis=1)

        if compatibility_mode:
            slope_angs = elevations
            earth_visibility = (elevations >= running_max_elevations)
        else:
            slope_angs = calculate_slope_angle_mesh(
                slopeangs_mesh_data.x_mesh,
                slopeangs_mesh_data.y_mesh,
                h_mesh_for_slopeangs,
                h_above_sea)
            earth_visibility = ((elevations >= running_max_elevations)
                                & (slope_angs >= 0))

        return elevations, running_max_elevations, earth_visibility, h_mesh, slope_angs

    def create_polar_maps(
            self,
            h_stand,
            h_above_land,
            mesh_data,
            facet_az_width,
            facet_r_width,
            freq,
            clutter_model=LandReflType.MODEL_3_NORAND,
            sea_state=4,
            r_min_for_earth_rcs=1.0,
            compatibility_mode=True,
            surface_type_fallback_to_relief=False,
            relief_fallback_to_zero=False,
    ):
        assert isinstance(mesh_data, PolarMeshData)

        if not self.terrain.dataset_available and not relief_fallback_to_zero:
            raise ReliefDataNotAvailableException

        slopeangs_mesh_data = PolarMeshData(
            r_min=mesh_data.r_min - mesh_data.r_step / 2.,
            r_max=mesh_data.r_max + mesh_data.r_step / 2.,
            n_r=mesh_data.n_r + 1,
            az_min=mesh_data.az_min - mesh_data.az_step / 2.,
            az_max=mesh_data.az_max + mesh_data.az_step / 2.,
            n_az=mesh_data.n_az + 1)

        # От плоскости, касательной к точке стояния РЛС, считаем
        # дальности до земного эллипсоида (т.е. до уровня моря)
        _, _, sealevel_distances = distances_to_sealevel(
            self._lat_stand,
            self._lon_stand,
            mesh_data.x_mesh,
            mesh_data.y_mesh)

        _, _, sealevel_distances_for_slopeangs = distances_to_sealevel(
            self._lat_stand,
            self._lon_stand,
            slopeangs_mesh_data.x_mesh,
            slopeangs_mesh_data.y_mesh)

        # Высота рельефа над уровнем моря в каждом из узлов сетки
        h_raw = self.terrain._land_height_interpolator.generate_polar_pic(
            self._lat_stand,
            self._lon_stand,
            mesh_data.r_min,
            mesh_data.r_max,
            mesh_data.n_r,
            mesh_data.az_min,
            mesh_data.az_max,
            mesh_data.n_az)

        # Высота рельефа над уровнем моря в узлах вспомогательной сетки для
        # расчёта углов падения луча
        h_raw_for_slopeangs = self.terrain._land_height_interpolator.generate_polar_pic(
            self._lat_stand,
            self._lon_stand,
            slopeangs_mesh_data.r_min,
            slopeangs_mesh_data.r_max,
            slopeangs_mesh_data.n_r,
            slopeangs_mesh_data.az_min,
            slopeangs_mesh_data.az_max,
            slopeangs_mesh_data.n_az)

        # Если присутствуют карты типа поверхности, производится
        # получение полярной карты типа поверхности
        if self.surface.dataset_available:
            surf_type_mesh = np.asarray(
                self.surface.surf_type_interpolator.generate_polar_pic(
                    self._lat_stand,
                    self._lon_stand,
                    mesh_data.r_min,
                    mesh_data.r_max,
                    mesh_data.n_r,
                    mesh_data.az_min,
                    mesh_data.az_max,
                    mesh_data.n_az,
                ), dtype=np.int32
            )
        # Если отсутствуют карты типа поверхности и указан откат на
        # карты, сформированные по картам рельефа
        elif surface_type_fallback_to_relief:
            surf_type_mesh = np.asarray(
                self.terrain.surf_type_interpolator.generate_polar_pic(
                    self._lat_stand,
                    self._lon_stand,
                    mesh_data.r_min,
                    mesh_data.r_max,
                    mesh_data.n_r,
                    mesh_data.az_min,
                    mesh_data.az_max,
                    mesh_data.n_az,
                ), dtype=np.int32
            )
        # Если откат запрещен, исключение
        else:
            raise SurfaceTypeDataNotAvailableException

        # Высота центра антенны над уровнем моря
        h_above_sea = h_stand + h_above_land

        # Вот тут-то и происходит расчёт основной части полярных карт
        elevations, running_max_elevations, earth_visibilities, h_mesh, slope_angs = self._calc_els_and_vis(
            mesh_data, slopeangs_mesh_data, h_raw, h_raw_for_slopeangs,
            h_above_sea, sealevel_distances,
            sealevel_distances_for_slopeangs, compatibility_mode)

        # А по карте видимостей и углов падения уже идёт расчёт ЭПР фацетов
        if compatibility_mode:
            facet_r_mesh = mesh_data.range_mesh - mesh_data.r_step
        else:
            facet_r_mesh = mesh_data.range_mesh
        surf_rcs_mesh = surface_rcs_per_unit(
            mesh_data.az_mesh, facet_r_mesh, surf_type_mesh,
            earth_visibilities, slope_angs, facet_az_width,
            facet_r_width, r_min_for_earth_rcs, freq, clutter_model,
            h_above_sea, sea_state)

        polar_maps = {}
        polar_maps[BgType.HEIGHTS_ABOVE_SEALEVEL] = h_raw
        polar_maps[BgType.HEIGHTS_ABOVE_PLAIN] = h_mesh
        polar_maps[BgType.ELEVATION_ANGLES] = elevations
        polar_maps[BgType.EARTH_VISIBILITY] = earth_visibilities
        polar_maps[BgType.SLOPE_ANGLES] = slope_angs
        polar_maps[BgType.SCREEN_ANGLES] = running_max_elevations
        polar_maps[BgType.TYPE_OF_SURFACE] = surf_type_mesh
        polar_maps[BgType.SURFACE_RCS] = surf_rcs_mesh

        return polar_maps, mesh_data, h_above_sea
