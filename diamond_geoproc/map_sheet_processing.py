import os, sys
import pandas as pd
import rasterio
from osgeo import gdal
import rtree.index
import pyproj
import zipfile
import logging

import matplotlib.pyplot as plt
import matplotlib.patches as patches

from diamond_geoproc.misc import TopoCoordTransformer



def show_land_coverage(coverage_dataframe, show_text_marks=False):
    """
    Показать карту покрытия земли матрицами свойств местности из pandas.DataFrame.

    Args:
        coverage_dataframe(pandas.DataFrame): должен содержать колонки "fname", а также
            "east", "west", "south", "north" в градусах.
        show_text_marks(bool): показывать ли имена файлов на карте.
    """
    cur_dir = os.path.abspath(os.path.dirname(__file__))
    earth_pic = plt.imread(os.path.join(cur_dir, "land_shallow_topo_2048.tif"))
    plt.imshow(earth_pic, extent=[-180, 180, -90, 90])
    ax = plt.gca()

    for index, row in coverage_dataframe.iterrows():
        east, west, north, south, = float(row["east"]), float(row["west"]), float(row["north"]), float(row["south"])
        rect = patches.Rectangle(
            (west, south), east - west, north - south, linewidth=1, edgecolor='r', facecolor='none')
        ax.add_patch(rect)
        if show_text_marks:
            plt.text((west + east) / 2, (north + south) / 2, row["fname"], fontsize=8,
                 horizontalalignment="center", verticalalignment="center", color="r")
    plt.show()


def select_geotiff_names(coverage_index, lon, lat, search_radius=None):
    """
    Args:
        coverage_index(rtree.index.Index): an index
        lon(float):
        lat(float):
        search_radius(float):
    Returns:
        list: a list of (fname, tifname) of geotiffs in index
    """
    if search_radius is None:
        min_lon, max_lon, min_lat, max_lat = lon, lon, lat, lat
    else:
        coord_transformer = TopoCoordTransformer(lon, lat)

        lons, lats, _ = coord_transformer.xyz_topo_to_lonlatalt(
            [-search_radius, search_radius, -search_radius, search_radius],
            [-search_radius, -search_radius, search_radius, search_radius], [0, 0, 0, 0])

        min_lon, max_lon, min_lat, max_lat = min(lons), max(lons), min(lats), max(lats)

    tifs_to_load = [elm.object for elm in (coverage_index.intersection((min_lon, min_lat, max_lon, max_lat),
                                                                       objects=True))]
    return tifs_to_load


def create_coverage_dataframe(path_to_geotiffs):
    """
        Читает файлы из директории (.tif и .zip, содержащие .tif), cоздаёт pandas.Dataframe, содержащий информацию о
            покрытии Земли данными геотифками. NB: в каждом zip-архиве должно быть ровно по одной геотифке!
            В датафрейме будут следующие пункты: "fname" - имя файла, "tifname" - имя тифки в архиве (если это архив),
            "west", "east", "south", "north" в градусах.
    Args:
        path_to_geotiffs: путь к директории с геотифками
    :return:
    """
    coverage_dict = {"fname": [], "tifname": [], "west": [], "east": [], "south": [], "north": []}

    fnames = os.listdir(path_to_geotiffs)
    for i, name in enumerate(fnames):
        logging.debug(i, "/", len(fnames))
        if name.endswith(".zip"):
            path_to_zip = os.path.join(path_to_geotiffs, name)
            zf = zipfile.ZipFile(path_to_zip, "r", zipfile.ZIP_DEFLATED)
            fnames_in_zip = [z.filename for z in zf.infolist() if z.filename.endswith(".tif")]
            assert len(fnames_in_zip) == 1
            path_to_raster = "/vsizip/%s/%s" % (os.path.join(path_to_geotiffs, name),  fnames_in_zip[0])
            coverage_dict["tifname"].append(fnames_in_zip[0])
        elif name.endswith("tif") or name.endswith("tiff"):
            path_to_raster = os.path.join(path_to_geotiffs, name)
            coverage_dict["tifname"].append(None)
        else:
            continue

        dataset = rasterio.open(path_to_raster)
        coverage_dict["fname"].append(name)
        coverage_dict["west"].append(dataset.bounds.left)
        coverage_dict["east"].append(dataset.bounds.right)
        coverage_dict["north"].append(dataset.bounds.top)
        coverage_dict["south"].append(dataset.bounds.bottom)
    df_to_write = pd.DataFrame.from_dict(coverage_dict)
    return df_to_write


def load_coverage_dataframe(path_to_csv):
    return pd.read_csv(path_to_csv, sep='\t')


def create_coverage_index(coverage_df):
    """
    Создаёт пространственный индекс из датафрейма с картой покрытия, в индексе лежат пары (fname, tifname)
    """

    idx = rtree.index.Index()
    for index, row in coverage_df.iterrows():
        east, west, north, south = float(row["east"]), float(row["west"]), float(row["north"]), float(row["south"])
        # left, bottom, right, top
        idx.insert(id=index, coordinates=(west, south, east, north), obj=(row["fname"], row["tifname"]))
    return idx


if __name__ == "__main__":

    # df = create_coverage_dataframe("D:/MONGOLIA/GIS/geotiffs_from_argunsoft/mmh100")
    # df.to_csv("D:/MONGOLIA/GIS/geotiffs_from_argunsoft/mmh100/coverage.csv", sep="\t", header=True, index=False)

    # df = create_coverage_dataframe("D:/MONGOLIA/GIS/geotiffs_from_argunsoft/mmr250")
    # df.to_csv("D:/MONGOLIA/GIS/geotiffs_from_argunsoft/mmr250/coverage.csv", sep="\t", header=True, index=False)

    map_dir_path = "/home/art/python_projects/srtm_dumps_single"
    coverage_csv_path = os.path.join(map_dir_path, "coverage.csv")
    coverage_df = pd.read_csv(coverage_csv_path, sep='\t')

    show_land_coverage(coverage_df)

    moscow_lon, moscow_lat = 20.618423, 38.751244
    search_radius = 0e3  # in meters
    idx = create_coverage_index(coverage_df)

    geotiffs_to_load = select_geotiff_names(idx, moscow_lon, moscow_lat, search_radius)
    abs_fnames = [("/vsizip/%s/%s" % (os.path.join(map_dir_path, elm[0]), elm[1])) for elm in geotiffs_to_load]
    gdal.BuildVRT("temp.vrt", abs_fnames)

    in_dataset = rasterio.open("temp.vrt")
    print(in_dataset)
    print(in_dataset.bounds)

    print(geotiffs_to_load)

    # print(coverage_df)