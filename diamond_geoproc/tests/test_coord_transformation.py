import pytest
import logging
import numpy as np
from diamond_geoproc.misc import TopoCoordTransformer
import pyproj

logger = logging.Logger(name="test_coord_transformation_logger", level=logging.DEBUG)

lat_stand = 0
lon_stand = 0
z_stand = 0
transformer = TopoCoordTransformer(lat_stand=lat_stand, lon_stand=lon_stand, z_abovesea_stand=z_stand)



def test_lla_to_ecef():  # _and_ecef_to_lla
    # compared with http://www.oc.nps.edu/oc2902w/coord/llhxyz.htm
    lat0, lon0, alt0 = 15, 30, 1000
    x0_ref, y0_ref, z0_ref = 5337.449e3, 3081.578e3, 1640.359e3 * TopoCoordTransformer._z_correction_ratio
    x0, y0, z0 = transformer.lonlatalt_to_xyz_geocent(lon0, lat0, alt0)
    print(x0, y0, z0)

    assert y0.shape == y0.shape

    np.testing.assert_allclose(x0, x0_ref, rtol=1e-6)
    np.testing.assert_allclose(y0, y0_ref, rtol=1e-6)
    np.testing.assert_allclose(z0, z0_ref, rtol=1e-6)

    lon_new, lat_new, alt_new = transformer.xyz_geocent_to_lonlatalt(x0, y0, z0)

    assert alt_new.shape == np.asarray(alt0).shape
    np.testing.assert_allclose(lon_new, lon0)
    np.testing.assert_allclose(lat_new, lat0)
    np.testing.assert_allclose(alt_new, alt0)


def test_lla_to_xyz():  # and back again
    lat = 55
    lon = 42  # degs
    z_abovesea = 500  # m
    x_topo, y_topo, z_topo = transformer.lonlatalt_to_xyz_topo(lat, lon, z_abovesea)
    lat_new, lon_new, z_abovesea_new = transformer.xyz_topo_to_lonlatalt(x_topo, y_topo, z_topo)

    assert lat_new.shape == np.asarray(lat).shape
    np.testing.assert_allclose(lat_new, lat)
    np.testing.assert_allclose(lon_new, lon)
    np.testing.assert_allclose(z_abovesea_new, z_abovesea)


def test_compare_with_pyproj():
    ortho_proj = pyproj.Proj(proj="ortho", ellps="WGS84", lat_0=lat_stand, lon_0=lon_stand)
    lla_proj = transformer._lla_proj

    lat = 90
    lon = 0
    alt = 0

    x_ortho_ref, y_ortho_ref = pyproj.transform(lla_proj, ortho_proj, lon, lat)
    x_topo, y_topo, z_topo = transformer.lonlatalt_to_xyz_topo(lon, lat, alt)

    assert np.asarray(x_topo).shape == np.asarray(y_topo).shape == np.asarray(x_ortho_ref).shape

    np.testing.assert_allclose(x_topo, x_ortho_ref, rtol=1e-6, atol=1e-6)
    np.testing.assert_allclose(y_topo, y_ortho_ref, rtol=1e-6, atol=1e-6)


def test_vel_conversion():
    lon = 0
    lat = 0
    alt = 1000

    topo_transformer = TopoCoordTransformer(lon_stand=lon, lat_stand=lat, z_abovesea_stand=alt)
    vx_geo, vy_geo, vz_geo = 100, 0, 0
    vx_topo, vy_topo, vz_topo = topo_transformer.xyz_geocent_to_xyz_topo(vx_geo, vy_geo, vz_geo, is_vel=True)

    assert vx_topo == vy_geo
    assert vy_topo == vz_geo
    assert vz_topo == vx_geo

    vx_geo_2, vy_geo_2, vz_geo_2 = topo_transformer.xyz_topo_to_xyz_geocent(vx_topo, vy_topo, vz_topo, is_vel=True)
    assert vx_geo_2 == vx_geo
    assert vy_geo_2 == vy_geo
    assert vz_geo_2 == vz_geo