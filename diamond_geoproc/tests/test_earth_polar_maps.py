from pathlib import Path

import numpy as np
import pytest

from diamond_geoproc.background_generation import PolarMeshData, BgType
from diamond_geoproc.earth_polar_map_api import EarthPolarMap
from diamond_geoproc.surface_map_api import SurfaceMapLoader
from diamond_geoproc.terrain_map_api import TerrainMapLoader, TerrainMap


@pytest.fixture
def srtm_dumps_dir():
    path = Path(__file__).parents[3] / "srtm_dumps"
    return str(path.absolute())


@pytest.fixture
def surface_type_dumps_dir():
    path = Path(__file__).parents[3] / "surface_type_dumps"
    return str(path.absolute())


def test_polar_map_generation(srtm_dumps_dir, surface_type_dumps_dir):
    lon, lat, h_stand = 30, 50, 200
    terrain_map_loader = TerrainMapLoader(srtm_dumps_dir,
                                          surface_type_fallback_value=False,
                                          relief_fallback_to_zero=False)
    surface_map_loader = SurfaceMapLoader(surface_type_dumps_dir)

    r_step = 100
    r_min = 100
    n_r = 500
    r_max = r_min + r_step * (n_r - 1)

    az_min = 0
    n_az = 360
    az_step = 1
    az_max = az_min + az_step * (n_az - 1)
    h_ant = 200

    # Задаём параметры сетки в полярных координатах (азимут-дальность)
    mesh_data = PolarMeshData(r_min=r_min, r_max=r_max, n_r=n_r,
                              az_min=az_min, az_max=az_max, n_az=n_az)

    # Создаём сам TerrainMap-объект (виртуальный растр, сделанный из геотифок,
    # находящихся в заданном радиусе от точки стояния РЛС)
    termap = terrain_map_loader.create_terrain_map(lon, lat, r_max)
    surfmap = surface_map_loader.create_surface_map(lon, lat, r_max)

    # Задаём размер фацета (обычно выбирают исходя из ширины ДНА)
    facet_az_width, facet_r_width = 80, 100
    freq = 1.e9

    earth_polar_map = EarthPolarMap(lon, lat, termap, surfmap)
    polar_maps, mesh_data, h_above_sea = earth_polar_map.create_polar_maps(
        h_stand, h_ant, mesh_data, facet_az_width, facet_r_width, freq,
        compatibility_mode=False, surface_type_fallback_to_relief=False,
        relief_fallback_to_zero=False
    )

    assert isinstance(polar_maps[BgType.HEIGHTS_ABOVE_SEALEVEL], np.ndarray)
    assert isinstance(polar_maps[BgType.HEIGHTS_ABOVE_SEALEVEL], np.ndarray)
    assert isinstance(polar_maps[BgType.HEIGHTS_ABOVE_PLAIN], np.ndarray)
    assert isinstance(polar_maps[BgType.ELEVATION_ANGLES], np.ndarray)
    assert isinstance(polar_maps[BgType.EARTH_VISIBILITY], np.ndarray)
    assert isinstance(polar_maps[BgType.SLOPE_ANGLES], np.ndarray)
    assert isinstance(polar_maps[BgType.SCREEN_ANGLES], np.ndarray)
    assert isinstance(polar_maps[BgType.TYPE_OF_SURFACE], np.ndarray)
    assert isinstance(polar_maps[BgType.SURFACE_RCS], np.ndarray)
