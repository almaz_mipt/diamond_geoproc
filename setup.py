from setuptools import setup, find_packages

setup(
    name="diamond_geoproc",
    version="0.0.0",
    author="S.P.Q.R.",
    author_email="alexey.golovizin@yandex.ru",
    description="TODO",
    license="BSD",
    keywords="TODO",
    packages=find_packages(exclude=["*.tests", "*.tests.*", "tests.*", "tests"])
)
