import numpy as np
import rasterio.transform
import rasterio.plot
import rasterio.shutil
import tempfile

from diamond_geoproc.map_sheet_processing import *
from diamond_geoproc.background_interpolation import LatLonInterpolator


class SurfaceMap:
    def __init__(self, lon, lat, dataset):
        self._lon_stand = float(lon)
        self._lat_stand = float(lat)

        if dataset:
            self.dataset_available = True
            # READING RASTER DATA
            assert isinstance(dataset, rasterio.DatasetReader)
            self._src_raster = dataset.read(1)[::-1, :]

            self._raster_lat_min = dataset.bounds.bottom
            self._raster_lon_min = dataset.bounds.left

            self._lat_step, self._lon_step = dataset.res
            self._n_raster_lats, self._n_raster_lons = self._src_raster.shape

            self._raster_lat_max = self._raster_lat_min + self._lat_step * (self._n_raster_lats - 1)
            self._raster_lon_max = self._raster_lon_min + self._lon_step * (self._n_raster_lons - 1)

            # Интерполятор, возвращающий тип поверхности в заданных точках
            self.surf_type_interpolator = LatLonInterpolator(
                self._raster_lat_min,
                self._raster_lat_max,
                self._raster_lon_min,
                self._raster_lon_max,
                self._src_raster,
                take_nearest=True,
                fill_value=0,
            )
        else:
            self.dataset_available = False
            self.surf_type_interpolator = LatLonInterpolator(
                0, 90, 0, 90, np.asarray([[0, 0], [0, 0]]),
                take_nearest=False, fill_value=0)


class SurfaceMapLoader:
    def __init__(self, map_dir_path):
        self._map_dir_path = map_dir_path
        self._coverage_info_path = os.path.join(map_dir_path, "coverage.csv")
        df = load_coverage_dataframe(self._coverage_info_path)
        self._idx = create_coverage_index(df)

        self._temp_dir = tempfile.TemporaryDirectory()

    def create_surface_map(self, lon_stand, lat_stand, r_max):
        tifs_to_load = select_geotiff_names(self._idx, lon_stand, lat_stand, r_max)
        if not tifs_to_load:
            return SurfaceMap(lon_stand, lat_stand, None)

        abs_fnames = []
        for file_name, zip_name in tifs_to_load:
            if isinstance(zip_name, str) or not np.isnan(zip_name):
                abs_fnames.append("/vsizip/%s/%s" % (os.path.join(self._map_dir_path, file_name), zip_name))
            else:
                abs_fnames.append("%s" % (os.path.join(self._map_dir_path, file_name)))

        virtual_raster_path = os.path.join(self._temp_dir.name, "temp_surf.vrt")
        gdal.BuildVRT(virtual_raster_path, abs_fnames)
        return SurfaceMap(lon_stand,
                          lat_stand,
                          rasterio.open(virtual_raster_path))
