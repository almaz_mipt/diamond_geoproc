"""Simple height matrix extractor.

This module is used for extraction of height matrices from GeoTIFF
files with accordance to trajectory given as points on the surface
of the Earth.

"""

import os
from zipfile import ZipFile

import numpy as np
from osgeo import gdal

from diamond_geoproc.background_generation import ReliefDataNotAvailableException


class Point(object):
    """Represents one point on the surface.

    Attributes:
        lat (float): Point latitude, degrees.
        long (float): Point longitude, degrees.
        x (float): Abscissa (equals to the longitude).
        y (Float): Ordinate (equals to the latitude).

    """
    def __init__(self, lat, long):
        if not (-180 <= lat <= 180 and -180 <= long <= 180):
            raise ValueError('Coordinates are incorrect')

        self.lat = lat
        self.long = long

        self.x = self.long
        self.y = self.lat


class Rectangle(object):
    """Represents an area for data gathering.

    On initialization vertices are sorted in clockwise order.

    Args:
        vertices (list): List of rectangle vertices as Points.

    Attributes:
        vertices (list): List of rectangle vertices as Points.

    """
    def __init__(self, vertices):
        pts = [[vertices[0].x, vertices[0].y],
               [vertices[1].x, vertices[1].y],
               [vertices[2].x, vertices[2].y],
               [vertices[3].x, vertices[3].y]]
        # Sort points in clockwise order around of this point.
        point_origin = [(vertices[0].x + vertices[2].x) / 2,
                        (vertices[0].y + vertices[2].y) / 2]
        # Sort starting from this vector
        refvec = [0, 1]

        def clockwise_angle(point):
            # Vector between point and the origin: v = p - o
            vector = [point[0] - point_origin[0], point[1] - point_origin[1]]
            # Length of vector: ||v||
            lenvector = np.hypot(vector[0], vector[1])
            # If length is zero there is no angle
            if lenvector == 0:
                return -np.pi, 0
            # Normalize vector: v/||v||
            normalized = [vector[0] / lenvector, vector[1] / lenvector]
            # x1*x2 + y1*y2
            dotprod = normalized[0] * refvec[0] + normalized[1] * refvec[1]
            # x1*y2 - y1*x2
            diffprod = refvec[1] * normalized[0] - refvec[0] * normalized[1]
            angle = np.arctan2(diffprod, dotprod)
            # Negative angles represent counter-clockwise angles
            # so we need to subtract them
            # from 2*pi (360 degrees)
            if angle < 0:
                return 2 * np.pi + angle, lenvector
            return angle, lenvector

        pts = sorted(pts, key=clockwise_angle)
        self.vertices = [Point(pts[0][1], pts[0][0]),
                         Point(pts[1][1], pts[1][0]),
                         Point(pts[2][1], pts[2][0]),
                         Point(pts[3][1], pts[3][0])]


def build_vrt(dump_path, vrt_filename):
    """ The build_vrt function creates virtual raster
    in the folder with compressed GeoTiff rasters.
    Args:
      dump_path (str): A path to the folder with zip-compressed GeoTIFF
           rasters.

    """
    rasters_paths = []
    for filename in os.listdir(dump_path):
        if os.path.splitext(filename)[-1] == '.tif':
            # Append tif file relative path to the rasters_paths.
            # For raster in tif file path can be relative, for zip-compressed
            # raster it must be absolute.
            tif_filepath = os.path.join(dump_path, filename)
            rasters_paths.append(tif_filepath)
        elif os.path.splitext(filename)[-1] == '.zip':
            archive_filepath = os.path.join(dump_path, filename)
            archive = ZipFile(archive_filepath)
            # Get the list of compressed files
            list_of_compressed_files = archive.namelist()
            # Find first *.tif file in the archive and append its absolute path
            # to the rasters_paths
            for compressed_file_name in list_of_compressed_files:
                if os.path.splitext(compressed_file_name)[-1] == '.tif':
                    abs_tif_filepath = ('/vsizip/'
                            + (os.path.join(os.path.abspath(archive_filepath),
                                            compressed_file_name)))
                    rasters_paths.append(abs_tif_filepath)

    if len(rasters_paths) > 0:
        vrt_options = gdal.BuildVRTOptions(
            resampleAlg='nearest',
            addAlpha=False,
            VRTNodata=0,
        )
        gdal.BuildVRT(os.path.join(dump_path, vrt_filename),
                      rasters_paths,
                      options=vrt_options)
    else:
        raise ReliefDataNotAvailableException('No rasters in the path')


def check_if_points_are_inside_of_rectangle(ys, xs, rectangle):
    """Function checks if points are inside of a rectangle.
    Rectangle vertices must be sorted in clockwise order.

    Args:
        ys (ndarray): Ordinates of points.
        longs (ndarray): Abscissas of points.
        rectangle (Rectangle): Rectangle with vertices.

    Returns:
        mask (ndarray): A mask, True for points inside rectangle,
            False for points outside rectangle.

    """
    d1 = ((rectangle.vertices[1].x - rectangle.vertices[0].x) * (ys - rectangle.vertices[0].y) -
          (xs - rectangle.vertices[0].x) * (rectangle.vertices[1].y - rectangle.vertices[0].y)) < 0
    d2 = ((rectangle.vertices[2].x - rectangle.vertices[1].x) * (ys - rectangle.vertices[1].y) -
          (xs - rectangle.vertices[1].x) * (rectangle.vertices[2].y - rectangle.vertices[1].y)) < 0
    d3 = ((rectangle.vertices[3].x - rectangle.vertices[2].x) * (ys - rectangle.vertices[2].y) -
          (xs - rectangle.vertices[2].x) * (rectangle.vertices[3].y - rectangle.vertices[2].y)) < 0
    d4 = ((rectangle.vertices[0].x - rectangle.vertices[3].x) * (ys - rectangle.vertices[3].y) -
          (xs - rectangle.vertices[3].x) * (rectangle.vertices[0].y - rectangle.vertices[3].y)) < 0
    mask = d1 * d2 * d3 * d4
    return mask


def get_heightmat(dump_path, relief_rectangles, relief_fallback_to_zero=False):
    """The main function that extracts height matrices for
    the set relief rectangle.

    Args:
        dump_path (str): A path to the folder with zip-compressed GeoTIFF
            rasters.
        relief_rectangles (list): List of relief rectangles, vertices are in
            next order
            [((downLeft_lat, downLef_long), (downRight_lat, downRight_long),
             (topLeft_lat, topLeft_long), (topRight_lat, topRight_long))].

    Returns:
        result (list): A list of dicts with altitude,
            latitude and longitude masked arrays. Every dict contains
            masked arrays with latitude, longitude and altitude.
            Unmasked values are inside of rectangles.

    """
    if not dump_path:
        raise ValueError('No path specified')
    if not relief_rectangles:
        raise ValueError('No relief rectangles specified')

    # Virtual raster creation
    vrt_filename = 'coverage.vrt'
    if not os.path.exists(os.path.join(dump_path, vrt_filename)):
        build_vrt(dump_path, vrt_filename)
    ds = gdal.Open(os.path.join(dump_path, vrt_filename))
    # Params of the vitual raster
    longssize = ds.RasterXSize
    latssize = ds.RasterYSize
    geotransform = ds.GetGeoTransform()
    x_origin = geotransform[0]
    y_origin = geotransform[3]
    pixel_width = geotransform[1]
    pixel_height = geotransform[5]
    longs = x_origin + np.arange(0, longssize) * pixel_width
    lats = y_origin + np.arange(0, latssize) * pixel_height
    xEnd = longs[-1]
    yEnd = lats[-1]

    result = []
    # Iterate the rectangles
    for rectangle in relief_rectangles:
        # Get a rectangle object with vertices in clockwise order.
        rectangle = Rectangle([Point(rectangle[0][0],
                                     rectangle[0][1]),
                               Point(rectangle[2][0],
                                     rectangle[2][1]),
                               Point(rectangle[3][0],
                                     rectangle[3][1]),
                               Point(rectangle[1][0],
                                     rectangle[1][1])])

        # Min and max coordinates of the rectangle.
        min_x_rectangle = min(rectangle.vertices[0].x,
                              rectangle.vertices[1].x,
                              rectangle.vertices[2].x,
                              rectangle.vertices[3].x,)
        max_x_rectangle = max(rectangle.vertices[0].x,
                              rectangle.vertices[1].x,
                              rectangle.vertices[2].x,
                              rectangle.vertices[3].x,)
        min_y_rectangle = min(rectangle.vertices[0].y,
                              rectangle.vertices[1].y,
                              rectangle.vertices[2].y,
                              rectangle.vertices[3].y,)
        max_y_rectangle = max(rectangle.vertices[0].y,
                              rectangle.vertices[1].y,
                              rectangle.vertices[2].y,
                              rectangle.vertices[3].y,)

        # Check if there is enough data for the rectangle.
        if (min_x_rectangle < min(x_origin, xEnd) or
                max_x_rectangle > max(x_origin, xEnd) or
                min_y_rectangle < min(y_origin, yEnd) or
                max_y_rectangle > max(y_origin, yEnd)):
            raise ReliefDataNotAvailableException('Not enough data')

        # If shortest way between points intersects 180th meridian,
        # crop and concat data left and right from 180th meridian.
        # TODO better solution
        up_point = max(rectangle.vertices[0].lat,
                       rectangle.vertices[1].lat,
                       rectangle.vertices[2].lat,
                       rectangle.vertices[3].lat)
        down_point = min(rectangle.vertices[0].lat,
                         rectangle.vertices[1].lat,
                         rectangle.vertices[2].lat,
                         rectangle.vertices[3].lat)
        right_point = max(rectangle.vertices[0].long,
                          rectangle.vertices[1].long,
                          rectangle.vertices[2].long,
                          rectangle.vertices[3].long,)
        left_point = min(rectangle.vertices[0].long,
                         rectangle.vertices[1].long,
                         rectangle.vertices[2].long,
                         rectangle.vertices[3].long,)
        if (abs(rectangle.vertices[0].long - rectangle.vertices[2].long) >
                (180 - right_point) + (left_point - (-180))):
            row1 = int((min_y_rectangle - y_origin) / pixel_height)
            row2 = int((max_y_rectangle - y_origin) / pixel_height)
            col0 = 0
            col1 = int((min_x_rectangle - x_origin) / pixel_width)
            col2 = int((max_x_rectangle - x_origin) / pixel_width)
            col3 = longssize - 1

            data_left = ds.ReadAsArray(col0,
                                       row2,
                                       col1 - col0 + 1,
                                       row1 - row2 + 1)
            data_right = ds.ReadAsArray(col2,
                                        row2,
                                        col3 - col2 + 1,
                                        row1 - row2 + 1)
            longs_left, lats_left = np.meshgrid(longs[col0:col1 + 1],
                                                lats[row2:row1 + 1])
            longs_right, lats_right = np.meshgrid(longs[col2:col3 + 1],
                                                  lats[row2:row1 + 1])

            data = np.hstack([data_left, data_right])
            longs_grid = np.hstack([longs_left, longs_right])
            lats_grid = np.hstack([lats_left, lats_right])

        # And if shortest way between points intersects 90th parallel,
        # crop and concat data up and down from 90th parallel.
        # TODO better solution
        elif (abs(rectangle.vertices[0].lat - rectangle.vertices[2].lat) >
              (90 - up_point) + (down_point - (-90))):
            col1 = int((min_x_rectangle - x_origin) / pixel_width)
            col2 = int((max_x_rectangle - x_origin) / pixel_width)

            row0 = 0
            row1 = int((min_y_rectangle - y_origin) / pixel_height)
            row2 = int((max_y_rectangle - y_origin) / pixel_height)
            row3 = latssize - 1

            data_up = ds.ReadAsArray(col1,
                                     row1,
                                     col2 - col1 + 1,
                                     row0 - row1 + 1)
            data_down = ds.ReadAsArray(col1,
                                       row3,
                                       col2 - col1 + 1,
                                       row2 - row3 + 1)
            longs_up, lats_up = np.meshgrid(longs[col1:col2 + 1],
                                            lats[row1:row0 + 1])
            longs_down, lats_down = np.meshgrid(longs[col1:col2 + 1],
                                                lats[row3:row2 + 1])

            data = np.vstack([data_up, data_down])
            longs_grid = np.vstack([longs_up, longs_down])
            lats_grid = np.vstack([lats_up, lats_down])

        # In all other cases crop the data from rectangular area.
        else:
            row1 = int((min_y_rectangle - y_origin) / pixel_height)
            col1 = int((min_x_rectangle - x_origin) / pixel_width)
            row2 = int((max_y_rectangle - y_origin) / pixel_height)
            col2 = int((max_x_rectangle - x_origin) / pixel_width)
            data = ds.ReadAsArray(col1, row2, col2 - col1 + 1, row1 - row2 + 1)
            longs_grid, lats_grid = np.meshgrid(longs[col1:col2 + 1],
                                                lats[row2:row1 + 1])

        # Generate a mask with the same size as data
        # True for values inside rectangle, False for values outside rectangle
        mask = check_if_points_are_inside_of_rectangle(lats_grid,
                                                       longs_grid,
                                                       rectangle)
        # Invert the mask, in numpy True must mean masked value,
        # False - unmasked value.
        mask = np.invert(mask)
        # Append the dict with masked arrays.
        result.append({'altitude': np.ma.masked_array(data, mask),
                       'latitude': np.ma.masked_array(lats_grid, mask),
                       'longitude': np.ma.masked_array(longs_grid, mask),})

    return result
