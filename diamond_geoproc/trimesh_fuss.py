
import trimesh
import trimesh.creation
from shapely.geometry import Polygon
import numpy as np

import scipy as sp
import scipy.stats

import pyproj
from pyproj import Geod


def densify_polygon(lonlat_points, n_extra_points_per_section):
    """
        Args:
            lonlat_points:
            n_extra_points_per_section:
        Returns:
            numpy.ndarray: a new array of points of densified polygon
    """
    geoid = Geod("WGS84")

    points = []

    for i, point in enumerate(lonlat_points[:-1]):
        next_point = lonlat_points[i + 1]
        lon0, lat0 = point
        lon1, lat1 = next_point

        extra_points = geoid.npts(lon0, lat0, lon1, lat1, n_extra_points_per_section)

        points.append(point)
        points.append(extra_points)

    points.append(lonlat_points[-1])

    points = np.asarray(points)
    assert len(points.shape) == 2
    assert points.shape[0] > 3
    assert points.shape[1] == 2
    return points



if __name__ == "__main__":



    # print("^_^")
    #
    # poly = Polygon([(0, 0), (1, 0), (0.5, 0.5), (1, 1), (0, 1)])
    #
    # mesh = trimesh.creation.extrude_polygon(poly, 10)
    #
    # size = 1000 * 1000
    #
    # rv = sp.stats.uniform(0, 1)
    #
    # data_shape = (100 * 100,)
    #
    # x_data = sp.stats.uniform.rvs(size=data_shape)
    # y_data = sp.stats.uniform.rvs(size=data_shape)
    # z_data = sp.stats.uniform.rvs(size=data_shape)
    #
    # data = np.stack((x_data, y_data, z_data), axis=-1)
    # print(data.shape)
    #
    # # points = [(0.75, 0.8, 1), (0.75, 0.75, 1)]
    # out_data = mesh.contains(data)
    # print(out_data)
    # # mesh.show()


    lon0 = 0
    lat0 = 0

    lon1 = 30
    lat1 = 15

    n_pts = 100

    g = Geod(ellps='WGS84')
    print(g.npts(lon0, lat0, lon1, lat1, n_pts))

