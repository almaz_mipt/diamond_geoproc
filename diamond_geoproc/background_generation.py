# -*- coding: UTF-8 -*-

from enum import IntEnum, unique, auto
from dataclasses import dataclass, field

import numpy as np

import scipy as sp
import scipy.io
import scipy.interpolate

import matplotlib.pyplot as plt
from collections import OrderedDict

from diamond_geoproc.misc import distances_to_sealevel, xy_to_lonlat
from diamond_geoproc.background_interpolation import LatLonInterpolator
from diamond_geoproc.slope_angle_calculations import *


class ReliefDataNotAvailableException(Exception):
    """Raised when the relief data is not available."""
    pass


class SurfaceTypeDataNotAvailableException(Exception):
    """Raised when the surface type data is not available."""
    pass


@unique
class BgType(IntEnum):
    """
    Типы картинок в полярных координатах с данными о поверхности (взятыми по сетке по дальности и азимуту)
    """

    # Высоты центров фацетов над уровнем моря
    HEIGHTS_ABOVE_SEALEVEL = auto()
    # Высоты центров фацетов над плоскостью, касательной к уровню моря в широте и долготе РЛС
    HEIGHTS_ABOVE_PLAIN = auto()
    # Угломестныая координата (угол возвышения) для центров фацетов
    ELEVATION_ANGLES = auto()
    # Сетка углов закрытия (минимальный угол возвышения, где РЛС ещё что-то видит в этом месте)
    SCREEN_ANGLES = auto()
    # Видимость фацетов c позиции РЛС (бинарная маска True/False)
    EARTH_VISIBILITY = auto()
    # Видимость цели, летящей на некоторой высоте над поверхности, с позиции РЛС (бинарная маска)
    TARGET_VISIBILITY = auto()
    # Типы поверхностей фацетов
    TYPE_OF_SURFACE = auto()
    # ЭПР фацетов
    SURFACE_RCS = auto()
    # Углы падения луча на фацеты
    SLOPE_ANGLES = auto()


class ScreeningAnglesCard(object):
    def __init__(self, ang_min, ang_max, screening_el_angs, obstacle_distances, use_degrees=False):
        self.ang_min = float(ang_min)
        self.ang_max = float(ang_max)

        assert isinstance(screening_el_angs, np.ndarray)
        self.screening_el_angs = screening_el_angs

        assert isinstance(obstacle_distances, np.ndarray)
        self.obstacle_distances = obstacle_distances

        self.use_degrees = bool(use_degrees)  # degrees if True, radians if False


class PolarReliefInfo(object):
    def __init__(self, polar_bg_dict, screening_angles_card):
        self.polar_bg_dict = polar_bg_dict  # { (bg_type, bg_alt) --> polar_image}
        self.screening_angles_card = screening_angles_card


@dataclass
class PolarMeshData():
    """
        Вспомогательный класс для работы с полярной сеткой
    """

    r_min: float
    r_max: float
    az_min: float
    az_max: float
    n_r: int
    n_az: int

    ranges: np.ndarray = field(init=False, repr=False)
    r_step: float = field(init=False)
    azimuths: np.ndarray = field(init=False, repr=False)
    az_step: float = field(init=False)

    az_mesh: np.ndarray = field(init=False, repr=False)
    range_mesh: np.ndarray = field(init=False, repr=False)

    x_mesh: np.ndarray = field(init=False, repr=False)
    y_mesh: np.ndarray = field(init=False, repr=False)

    def __post_init__(self):
        self.r_min = float(self.r_min)
        self.r_max = float(self.r_max)
        self.az_min = float(self.az_min)
        self.az_max = float(self.az_max)

        self.n_r = int(self.n_r)
        self.n_az = int(self.n_az)

        self.ranges = np.linspace(self.r_min, self.r_max, self.n_r)
        self.r_step = (self.r_max - self.r_min) / (self.n_r - 1)

        self.azimuths = np.linspace(self.az_min, self.az_max, self.n_az)
        self.az_step = (self.az_max - self.az_min) / (self.n_az - 1)

        self.az_mesh, self.range_mesh = np.meshgrid(self.azimuths, self.ranges,
                                                    indexing="ij")  # Коорд. узлов в (az,r)

        self.x_mesh = self.range_mesh * np.sin(self.az_mesh)  # Коорд. узлов в XY
        self.y_mesh = self.range_mesh * np.cos(self.az_mesh)


class PolarBackgroundGenerator(object):
    def __init__(self, dem_raster_data, dem_lat_min, dem_lat_step, dem_lon_min, dem_lon_step,
                 dem_nodata_threshold=-1000, local_heights=None, type_of_surface_data=None):
        self._dem_raster_data = dem_raster_data
        assert isinstance(self._dem_raster_data, np.ndarray)

        self._n_lats, self._n_lons = self._dem_raster_data.shape

        self._dem_lat_min = float(dem_lat_min)
        self._dem_lat_step = float(dem_lat_step)
        self._dem_lat_max = dem_lat_min + dem_lat_step * self._n_lats

        self._dem_lon_min = float(dem_lon_min)
        self._dem_lon_step = float(dem_lon_step)
        self._dem_lon_max = dem_lon_min + dem_lon_step * self._n_lons

        self._dem_nodata_threshold = float(dem_nodata_threshold)

        # constants

        self.RE = 6371000.  # Mean Earth radius in m
        self.Reff = 4. / 3. * self.RE  # Effective Earth radius in m

        raw_raster_data = np.clip(self._dem_raster_data, self._dem_nodata_threshold, None)

        # FIXME: magic number!!
        is_sea_raster_mask = raw_raster_data < -150  # Sea is read as -200 data.

        raster_data_with_sealevel = raw_raster_data.copy()
        raster_data_with_sealevel[np.where(is_sea_raster_mask)] = 0

        self._is_sea_interpolator = LatLonInterpolator(
            self._dem_lat_min, self._dem_lat_max, self._dem_lon_min, self._dem_lon_max, is_sea_raster_mask,
            take_nearest=True, fill_value=0)

        self._land_height_interpolator = LatLonInterpolator(
            self._dem_lat_min, self._dem_lat_max, self._dem_lon_min, self._dem_lon_max, raster_data_with_sealevel,
            take_nearest=False, fill_value=0)

    def _calc_elevations_and_visibilities(
            self, mesh_data, slopeangs_mesh_data, h_raw_mesh, h_raw_mesh_for_slopeangs, h_stand_above_earth,
            sealevel_distances, sealevel_distances_for_slopeangs, surf_height_from_earth=0.0):

        standpoint_above_sealevel = h_raw_mesh[0, 0] + h_stand_above_earth

        angs_from_vert = np.arctan(mesh_data.ranges / self.Reff)
        h_mesh = h_raw_mesh * np.cos(angs_from_vert[None, :]) + sealevel_distances

        angs_from_vert_for_slopeangs = np.arctan(slopeangs_mesh_data.ranges / self.Reff)
        h_mesh_for_slopeangs = h_raw_mesh_for_slopeangs * \
                               np.cos(angs_from_vert_for_slopeangs[None, :]) + sealevel_distances_for_slopeangs

        slope_angs = calculate_slope_angle_mesh(slopeangs_mesh_data.x_mesh, slopeangs_mesh_data.y_mesh,
                                                h_mesh_for_slopeangs, standpoint_above_sealevel)

        elevations = np.zeros_like(mesh_data.range_mesh)
        elevations[:, 0] = -np.pi / 2
        elevations[:, 1:] = np.arctan((h_mesh[:, 1:] - standpoint_above_sealevel) / mesh_data.range_mesh[:, 1:])
        running_max_elevations = np.maximum.accumulate(elevations, axis=1)

        earth_visibility = (elevations >= running_max_elevations) & (slope_angs >= 0)

        if surf_height_from_earth == 0:
            return elevations, running_max_elevations, earth_visibility, h_mesh, slope_angs
        else:
            assert False

        # h_target_mesh = (h_raw_mesh + surf_height_from_earth) * np.cos(angs_from_vert[None, :]) + sealevel_distances
        # target_els = np.zeros_like(h_target_mesh)
        # target_els[:, 0] = -np.pi / 2
        # target_els[:, 1:] = np.arctan((h_target_mesh[:, 1:] - standpoint_above_sealevel) / range_mesh[:, 1:])
        # target_visibility_mesh = target_els >= slope_angs_running_max
        # return slope_angs, slope_angs_running_max, target_visibility_mesh, h_mesh

    def get_polar_relief_info(self, bg_specs, lat_stand, lon_stand, h_stand, r_min, r_max, n_r, az_min, az_max, n_az):
        """
        Args:
            bg_specs:
            lat_stand:
            lon_stand:
            r_min:
            r_max:
            n_r:
            az_min:
            az_max:
            n_az:
        Returns:
            PolarReliefInfo
        """

        # Строим сетку, в узлах которой впоследствие будем считать значения ЭПР
        # Для расчётов углов падения луча в узлах этой сетки строим дополнительную сетку со сдвигом на половинку шага
        # (с тем, чтобы считать угол падения в центре её полигонов).

        mesh_data = PolarMeshData(r_min=r_min, r_max=r_max, n_r=n_r,
                                  az_min=az_min, az_max=az_max, n_az=n_az)

        slopeangs_mesh_data = PolarMeshData(
            r_min=r_min - mesh_data.r_step / 2.,
            r_max=r_max + mesh_data.r_step / 2.,
            n_r=n_r + 1,
            az_min=az_min - mesh_data.az_step / 2.,
            az_max=az_max + mesh_data.az_step / 2.,
            n_az=n_az + 1)

        _, _, sealevel_distances = distances_to_sealevel(
            lat_stand, lon_stand, mesh_data.x_mesh, mesh_data.y_mesh)
        _, _, sealevel_distances_for_slopeangs = distances_to_sealevel(
            lat_stand, lon_stand,slopeangs_mesh_data.x_mesh, slopeangs_mesh_data.y_mesh)

        # Высота рельефа над уровнем моря в каждом из узлов сетки
        h_raw = self._land_height_interpolator.generate_polar_pic(lat_stand, lon_stand, r_min, r_max, n_r,
                                                                  az_min, az_max, n_az)

        # Высота рельефа над уровнем моря в узлах вспомогательной сетки для расчёта углов падения луча
        h_raw_for_slopeangs = self._land_height_interpolator.generate_polar_pic(
            lat_stand, lon_stand, slopeangs_mesh_data.r_min, slopeangs_mesh_data.r_max, slopeangs_mesh_data.n_r,
            slopeangs_mesh_data.az_min, slopeangs_mesh_data.az_max, slopeangs_mesh_data.n_az)


        is_sea_mask = self._is_sea_interpolator.generate_polar_pic(lat_stand, lon_stand, r_min, r_max, n_r,
                                                                   az_min, az_max, n_az)

        userfacetype = 6
        sea_terrain_id = 0
        surftype = np.zeros((n_az, n_r))
        surftype[:, :] = userfacetype
        surftype[np.where(is_sea_mask[:, 1:])] = sea_terrain_id


        elevations, running_max_elevations, earth_visibilities, h_mesh, slope_angs = \
            self._calc_elevations_and_visibilities(mesh_data, slopeangs_mesh_data, h_raw, h_raw_for_slopeangs, h_stand,
                                                   sealevel_distances, sealevel_distances_for_slopeangs)

        # Карточка углов углов закрытия
        screening_el_angs = running_max_elevations[:, -1]
        obstacle_distances = np.argmax(elevations, axis=1) * mesh_data.r_step
        scr_ang_card = ScreeningAnglesCard(az_min, az_max, screening_el_angs, obstacle_distances)

        # И словарик с подложками
        bg_dict = OrderedDict()
        bg_dict[(BgType.HEIGHTS_ABOVE_SEALEVEL, None)] = h_raw
        bg_dict[(BgType.HEIGHTS_ABOVE_PLAIN, None)] = h_mesh
        bg_dict[(BgType.ELEVATION_ANGLES, None)] = elevations
        bg_dict[(BgType.SCREEN_ANGLES, None)] = running_max_elevations
        bg_dict[(BgType.EARTH_VISIBILITY, None)] = earth_visibilities
        bg_dict[(BgType.TYPE_OF_SURFACE, None)] = surftype
        bg_dict[(BgType.SLOPE_ANGLES, None)] = slope_angs

        polar_relief_info = PolarReliefInfo(bg_dict, scr_ang_card)

        # Если нужно, рассчитать видимость цели в N метрах над землёй
        # for spec in bg_specs:
        #     if spec.bg_type == BgType.TARGET_VISIBILITY:
        #         altitude = spec.height_above_earth
        #         _, _, target_visibilities, _ = self._calc_els_and_vis(
        #             mesh_data.range_mesh, h_raw, h_stand, sealevel_distances, altitude)
        #         bg_dict[spec] = target_visibilities

        # Немного отладочной отрисовки
        # plt.pcolormesh(x_mesh[:, 10:], y_mesh[:, 10:], h_raw[:, 10:])
        # # plt.pcolormesh(x_mesh, y_mesh, target_visibilities)
        # #
        # x_obstacle = obstacle_distances * np.sin(az_angs)
        # y_obstacle = obstacle_distances * np.cos(az_angs)
        #
        # min_scr_angle, max_scr_angle = screening_el_angs.min() - 1e-6, screening_el_angs.max() + 1e-6
        # scr_angs_to_radiuses = (screening_el_angs - min_scr_angle) / (max_scr_angle - min_scr_angle) * r_max
        # x_screen = scr_angs_to_radiuses * np.sin(az_angs)
        # y_screen = scr_angs_to_radiuses * np.cos(az_angs)
        #
        # plt.plot(x_obstacle, y_obstacle)
        # plt.plot(x_screen, y_screen)
        # #
        # plt.show()

        return polar_relief_info


if __name__ == "__main__":

    in_data = sp.io.loadmat("recentconfig.mat")


    lat_stand = float(in_data["standpoint_lat"])
    lat_min = float(in_data["lat0_deg"])
    n_lats = int(in_data["nrows"])
    lat_step = float(in_data["rowstep_deg"])

    lon_stand = float(in_data["standpoint_lon"])
    lon_min = float(in_data["lon0_deg"])
    n_lons = int(in_data["ncols"])
    lon_step = float(in_data["colstep_deg"])

    h_stand = 8

    latlon_pic = in_data["height"].T[::-1, :]

    generator = PolarBackgroundGenerator(latlon_pic, lat_min, lat_step, lon_min, lon_step)
    bg_specs = []
    bg_specs.append((BgType.TARGET_VISIBILITY, 400))

    polar_relief_info = generator.get_polar_relief_info(bg_specs, lat_stand, lon_stand, h_stand, 0, 100e3, 1001, 0,
                                                        np.deg2rad(360), 361)

    image = polar_relief_info.polar_bg_dict[(BgType.SLOPE_ANGLES, None)] * polar_relief_info.polar_bg_dict[
        (BgType.EARTH_VISIBILITY, None)]

    plt.imshow(image[:, 10:])
    plt.show()

