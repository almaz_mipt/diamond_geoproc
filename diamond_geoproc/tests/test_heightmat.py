import os

from numpy.ma import masked_array
import pytest

from diamond_geoproc.heightmat import get_heightmat
from diamond_geoproc.heightmat import Point
from diamond_geoproc.heightmat import ReliefDataNotAvailableException


def test_point():
    lat = 10
    lon = 15
    p = Point(10, 15)
    assert p.x == lon
    assert p.y == lat


def test_data_out_of_range():
    rectangles = [((20.1, 43.1), (20.3, 43.1), (53.1, 43.2), (53.3, 43.2))]
    with pytest.raises(ReliefDataNotAvailableException):
        get_heightmat(
            os.path.join('test_dump2'),
            rectangles
        )


def test_output_format():
    rectangles = [((53, 43),
                   (53.1, 43),
                   (53, 43.1),
                   (53.1, 43.1)),
                  ((53.1, 43.1),
                   (53.3, 43.1),
                   (53.1, 43.2),
                   (53.3, 43.2))]
    heightmat_data = get_heightmat(os.path.join('test_dump2'),
                                   rectangles)
    assert type(heightmat_data) is list
    assert len(heightmat_data) == len(rectangles)
    for data_from_rectangle in heightmat_data:
        assert type(data_from_rectangle) is dict
        for data in data_from_rectangle.values():
            assert type(data) is masked_array
