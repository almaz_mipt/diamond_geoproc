import numpy as np



def calculate_slope_angle_mesh(x_mesh, y_mesh, z_mesh, pos_height, draw_plots=False):
    """
    Вычисляет массив углов между полигонами сетки и линией визирования от точки расположения РЛС до центров
        полигонов сетки. РЛС расположена в (0, 0, height). Возвращаемый массив имеет размерность
        [n_angles-1, n_ranges-1]

    Args:
        x_mesh(array-like): [i_ang, j_range]-shaped 2D mesh of x values
        y_mesh(array-like):
        z_mesh(array-like):
        pos_height(float):
    Returns:
        2D mesh of slope angles in radians
    """

    x_mesh = np.asarray(x_mesh, dtype=np.float64)
    y_mesh = np.asarray(y_mesh, dtype=np.float64)
    z_mesh = np.asarray(z_mesh, dtype=np.float64)
    assert x_mesh.shape == y_mesh.shape == z_mesh.shape
    assert len(x_mesh.shape) == 2

    pos_height = float(pos_height)
    vector_data = np.stack((x_mesh, y_mesh, z_mesh), axis=-1)  # dims: (angle, range, xyz)

    a1_corners = vector_data[:-1, :-1, :]  # Координаты углов фацетов
    a2_corners = vector_data[:-1, 1:, :]
    b1_corners = vector_data[1:, :-1, :]
    b2_corners = vector_data[1:, 1:, :]

    # Считаем нормаль-вектор из векторного произведения между A1->A2 и A2->B2
    vecs_1 = a1_corners - a2_corners
    vecs_2 = b2_corners - a2_corners
    normal_vecs = np.cross(vecs_1, vecs_2, axis=-1)  # Normal vectors to surface mesh cell
    normal_vecs /= np.linalg.norm(normal_vecs, axis=-1)[:, :, None]

    cell_centers = (a1_corners + a2_corners + b1_corners + b2_corners) * (1. / 4.)

    # Направления от центраов фацетов к РЛС
    directions_to_radar = np.asarray([0, 0, pos_height])[None, None, :] - cell_centers
    directions_to_radar /= np.linalg.norm(directions_to_radar, axis=-1)[:, :, None]

    # Находим угол через скалярное произведение нормалей к фацетам сетки и линии визирования "центр фацета -- РЛС".
    slope_angles = np.pi / 2 - np.arccos(np.clip(np.einsum('ijk,ijk->ij', normal_vecs, directions_to_radar), -1, 1))

    ############################
    if draw_plots:
        from mayavi.mlab import mesh, quiver3d, show

        mesh_plot = mesh(x_mesh, y_mesh, z_mesh, representation="wireframe")

        quiver_vecs = normal_vecs

        quiver_len = 1
        quiver_scale = 100
        normal_quiver_plot = quiver3d(
            cell_centers[:, :, 0], cell_centers[:, :, 1], cell_centers[:, :, 2],
            quiver_vecs[:, :, 0] * quiver_len, quiver_vecs[:, :, 1] * quiver_len, quiver_vecs[:, :, 2] * quiver_len,
            scale_factor=quiver_scale)

        show()
    ###############################

    return slope_angles


if __name__ == "__main__":

    angles = calculate_slope_angle_mesh([[0, 0], [0, .001]], [[0, .001], [0, 0]], [[0, 0], [0, 0]], 1)
    print(np.rad2deg(angles))