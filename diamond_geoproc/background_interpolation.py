# -*- coding: UTF-8 -*-

import numpy as np
import scipy as sp
import scipy.interpolate
import scipy.io

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import colors

import pyproj
import rasterio
import rasterio.transform
from rasterio.crs import CRS
from rasterio.warp import reproject, Resampling

from diamond_geoproc.misc import xy_to_lonlat


class PolarPicInterpolator(object):
    """
    This interpolator is used to transform POLAR --> CART images and POLAR --> LATLON images.
    """
    def __init__(self, ang_min, ang_max, r_min, r_max, height_matrix, add_one_more_angle=True, take_nearest=False,
                 fill_value=np.nan):
        """
        Args:
            ang_min(float): minimal angle in radians
            ang_max(float): maximal angle in radians
            r_min(float): minimal distance
            r_max(float): maximal distance
            height_matrix(array-like): a 2D matrix whose shape is interpreted as ``n_angles``-by-``n_radius``
            add_one_more_angle(bool): if `True`, we pad the height matrix with extra row taken from its beginning
                (for example, if we have height matrix with 360 angles from 0 to 359 degrees, now has covers 361 angles
                from 0 to 360)
            take_nearest(bool): if `True`, we will interpolate with nearest value, if `False`, we'll use linear
                interpolation
        """
        self._ang_min = float(ang_min)
        self._ang_max = float(ang_max)
        assert self._ang_max > self._ang_min
        self._r_min = float(r_min)
        self._r_max = float(r_max)
        assert self._r_max > self._r_min
        assert self._r_min >= 0

        self._heights = np.asarray(height_matrix)
        self._take_nearest = bool(take_nearest)
        assert len(self._heights.shape) == 2
        self._n_ang, self._n_r = self._heights.shape

        if self._take_nearest:
            method = "nearest"
        else:
            method = "linear"

        if bool(add_one_more_angle):
            ang_step = (self._ang_max - self._ang_min) / (self._n_ang - 1)
            self._ang_max += ang_step
            self._n_ang += 1
            self._heights = np.concatenate((self._heights, self._heights[None, 0, :]), axis=0)

        ang_grid_src = np.linspace(self._ang_min, self._ang_max, self._n_ang)
        r_grid_src = np.linspace(self._r_min, self._r_max, self._n_r)

        self._interpolator = sp.interpolate.RegularGridInterpolator(
            points=(ang_grid_src, r_grid_src), values=self._heights, bounds_error=False, method=method,
            fill_value=fill_value)

    def generate_cartesian_pic(self, x_min, x_max, n_x, y_min, y_max, n_y, take_nearest=None):
        """
        Args:
            x_min(float):
            x_max(float):
            n_x(float): number of X points
            y_min(float):
            y_max(float):
            n_y(float): number of Y points
            take_nearest(bool):
        Returns:
            :class:`numpy.ndarray`: a 2D matrix with shape ``n_y``-by-``n_x``

        """
        x_grid = np.linspace(float(x_min), float(x_max), int(n_x))
        y_grid = np.linspace(float(y_min), float(y_max), int(n_y))
        x_mesh, y_mesh = np.meshgrid(x_grid, y_grid)

        return self._generate_pic_from_xy_meshes(x_mesh, y_mesh, take_nearest)

    def _generate_pic_from_xy_meshes(self, x_mesh, y_mesh, take_nearest=None):
        complex_coords = np.zeros(shape=x_mesh.shape, dtype=np.complex128)
        complex_coords.real = x_mesh
        complex_coords.imag = y_mesh

        ang_mesh = (np.pi / 2. - np.angle(complex_coords) + 2 * np.pi) % (2 * np.pi)  # Azimuth: clockwisee from north
        r_mesh = np.abs(complex_coords)

        if take_nearest is None:
            method = None
        elif take_nearest == True:
            method = "nearest"
        elif take_nearest == False:
            method = "linear"
        else:
            assert False

        out_pic = self._interpolator(np.stack((ang_mesh, r_mesh), axis=-1), method=method)

        return out_pic

    def generate_latlon_pic(self, lat_min, lat_max, n_lats, lat_stand, lon_min, lon_max, n_lons, lon_stand,
                            take_nearest=None):
        """
        Args:
            lat_min(float): min latitude in degrees
            lat_max(float): max latutude in degrees
            n_lats(float): number of latitude points to generate
            lat_stand(float): latitude of the center of polar image for our radar (in degrees)
            lon_min(float):
            lon_max(float):
            n_lons(float):
            lon_stand(float):
        Returns:
            :class:`numpy.ndarray(dtype=float)`
        """

        lat_grid = np.linspace(float(lat_min), float(lat_max), int(n_lats))
        lon_grid = np.linspace(float(lon_min), float(lon_max), int(n_lons))
        lon_mesh, lat_mesh = np.meshgrid(lon_grid, lat_grid)

        latlon_proj = pyproj.Proj(init='EPSG:4326')

        ortho_proj = pyproj.Proj(proj="ortho", ellps='WGS84', datum="WGS84", lat_0=float(lat_stand),
                                 lon_0=float(lon_stand), units="m")

        x_mesh, y_mesh = pyproj.transform(latlon_proj, ortho_proj, lon_mesh, lat_mesh)

        return self._generate_pic_from_xy_meshes(x_mesh, y_mesh, take_nearest)

    def dump_latlon_pic(self, lat_stand, lon_stand, out_tif_path, pad_ratio=1.1, n_lats=2001, n_lons=2001, r_max=None,
                        take_nearest=None):
        """
        Сохраняет на диск GeoTIFFку в виде LonLat (EPSG-4326)

        Args:
            lat_stand(float): расположение РЛС по широте (в градусах)
            lon_stand(float): расположение РЛС по долготе (в градусах)
            out_tif_path(str): путь, куда будет сохранена GeoTIFFка
            pad_ratio(str): размер "полей" вокруг `r_max` (если 1.1 то 10% от `r_max`)
            n_lats(int): число шагов по широте в сохраняемой GeoTIFFке
            n_lons(int): число шагов по долготе в сохраняемой GeoTIFFке
            r_max(float): максимальный радиус (если не задан, то берём максимальный радиус интерполятора)
        """
        if r_max is None:
            r_max = self._r_max
        else:
            r_max = float(r_max)

        n_lats = int(n_lats)
        n_lons = int(n_lons)
        pad_ratio = float(pad_ratio)
        lat_stand = float(lat_stand)
        lon_stand = float(lon_stand)

        # SW, NW, SE, NE as [X, Y] points
        corner_points = np.asarray([[-1, -1], [-1, 1], [1, -1], [1, 1]], dtype=np.float64) * r_max * pad_ratio

        ortho_proj = pyproj.Proj(proj="ortho", ellps='WGS84', datum="WGS84", lat_0=lat_stand,
                                 lon_0=lon_stand, units="m")
        latlon_proj = pyproj.Proj(init='EPSG:4326')

        corner_lons, corner_lats = pyproj.transform(ortho_proj, latlon_proj, corner_points[:, 0], corner_points[:, 1])

        lat_min, lat_max = np.min(corner_lats), np.max(corner_lats)
        lon_min, lon_max = np.min(corner_lons), np.max(corner_lons)

        out_image = self.generate_latlon_pic(
            lat_min, lat_max, n_lats, lat_stand, lon_min, lon_max, n_lons, lon_stand, take_nearest)

        transform_matrix = rasterio.transform.from_bounds(lon_min, lat_min, lon_max, lat_max, n_lons, n_lats)

        dest_crs = rasterio.crs.CRS({'init': 'epsg:4326'})

        new_dataset = rasterio.open(out_tif_path, 'w', driver='GTiff', height=n_lats, width=n_lons, count=1,
                                    dtype=rasterio.float64, crs=dest_crs, transform=transform_matrix)
        new_dataset.write(out_image[::-1, :], 1)
        new_dataset.close()

    def draw_polar_pic(self, r_min, r_max, phi_min, phi_max):
        r_grid = np.linspace(r_min, r_max, self._n_r)
        phi_grid = np.linspace(phi_min, phi_max, self._n_ang)
        r_mesh, phi_mesh = np.meshgrid(r_grid, phi_grid)

        x_mesh = r_mesh * np.sin(phi_mesh)
        y_mesh = r_mesh * np.cos(phi_mesh)

        plt.pcolormesh(x_mesh, y_mesh, self._heights)


class LatLonInterpolator(object):
    def __init__(self, lat_min, lat_max, lon_min, lon_max, latlon_pic, take_nearest=False,
                 fill_value=np.nan):

        assert isinstance(latlon_pic, np.ndarray)
        n_lats, n_lons = latlon_pic.shape[:2]
        self._lat_grid = np.linspace(lat_min, lat_max, n_lats)
        self._lon_grid = np.linspace(lon_min, lon_max, n_lons)
        self._take_nearest = bool(take_nearest)
        self._fill_value = float(fill_value)
        if self._take_nearest:
            method = "nearest"
        else:
            method = "linear"

        self._interpolator = scipy.interpolate.RegularGridInterpolator(
            points=(self._lat_grid, self._lon_grid), values=latlon_pic, bounds_error=False, method=method,
            fill_value=self._fill_value)

    def _generate_pic_from_lonlat_meshes(self, lon_mesh, lat_mesh, take_nearest=None):
        if take_nearest is None:
            method = None
        elif take_nearest == True:
            method = "nearest"
        elif take_nearest == False:
            method = "linear"
        else:
            assert False

        out_pic = self._interpolator(np.stack((lat_mesh, lon_mesh), axis=-1), method=method)
        return out_pic

    def generate_cartesian_pic(self, lat_stand, lon_stand, x_min, x_max, n_x, y_min, y_max, n_y, take_nearest=None):

        x_grid = np.linspace(x_min, x_max, n_x)
        y_grid = np.linspace(y_min, y_max, n_y)

        x_mesh, y_mesh = np.meshgrid(x_grid, y_grid)


        lon_mesh, lat_mesh = xy_to_lonlat(lat_stand, lon_stand, x_mesh, y_mesh)

        cart_pic = self._generate_pic_from_lonlat_meshes(lon_mesh, lat_mesh, take_nearest)
        return cart_pic

    def generate_polar_pic(self, lat_stand, lon_stand,
                           r_min, r_max, n_r, phi_min, phi_max, n_phi, take_nearest=None):
        r_grid = np.linspace(r_min, r_max, n_r)
        phi_grid = np.linspace(phi_min, phi_max, n_phi)
        r_mesh, phi_mesh = np.meshgrid(r_grid, phi_grid)

        x_mesh = r_mesh * np.sin(phi_mesh)
        y_mesh = r_mesh * np.cos(phi_mesh)

        lon_mesh, lat_mesh = xy_to_lonlat(lat_stand, lon_stand, x_mesh, y_mesh)

        polar_pic = self._generate_pic_from_lonlat_meshes(lon_mesh, lat_mesh, take_nearest)
        return polar_pic

    def __call__(self, lons, lats):
        return self._interpolator((lons, lats))


def reproject_dataset_to_plane(dataset, standoff_point, left, right, bottom, top, x_size, y_size):
    x_left, x_right, y_bottom, y_top = left, right, bottom, top
    x_grid = np.linspace(x_left, x_right, x_size)
    y_grid = np.linspace(y_top, y_bottom, y_size)
    x_mesh_ortho, y_mesh_ortho = np.meshgrid(x_grid, y_grid)

    if dataset.count > 1:
        dest_raster = np.zeros((dataset.count, ) + x_mesh_ortho.shape, dtype=np.float64)
    else:
        dest_raster = np.zeros_like(x_mesh_ortho.shape, dtype=np.float64)

    dest_transform = rasterio.transform.Affine.from_gdal(x_left, (x_right - x_left) / (x_size - 1), 0,
                                                         y_top, 0, (y_bottom - y_top) / (y_size - 1))

    if len(standoff_point) != 2:
        raise ValueError("standoff_point should be a ((float_lon), (float_lon)) tuple!!")
    lat_center, lon_center = standoff_point

    ortho_crs = CRS({"proj": "ortho", "lat_0": lat_center, "lon_0": lon_center, "datum": "WGS84"})

    src_raster = dataset.read()

    reproject(
        source=src_raster,
        src_nodata=None,
        destination=dest_raster,
        src_transform=dataset.transform,
        src_crs=dataset.crs,
        dst_transform=dest_transform,
        dst_crs=ortho_crs,
        dst_nodata=None,
        resampling=Resampling.bilinear,
        fill=np.nan
    )

    if dataset.count > 1:
        dest_raster = np.moveaxis(dest_raster, 0, 2)
    return x_mesh_ortho, y_mesh_ortho, dest_raster



if __name__ == "__main__":

    in_data = sp.io.loadmat("recentconfig.mat")

    lat_stand = float(in_data["standpoint_lat"])  # standing point in degrees
    lon_stand = float(in_data["standpoint_lon"])

    lat_min = float(in_data["lat0_deg"])
    n_lats = int(in_data["nrows"])
    lat_step = float(in_data["rowstep_deg"])
    lat_max = lat_min + lat_step * (n_lats - 1)

    lon_min = float(in_data["lon0_deg"])
    n_lons = int(in_data["ncols"])
    lon_step = float(in_data["colstep_deg"])
    lon_max = lon_min + lon_step * (n_lons - 1)

    # Let's generate some surface in polar coordinates (phi, rho) with fixed step by phi and rho
    # r_step = float(in_data["rstep"])
    # n_r = float(in_data["nr"])
    # r_min, r_max = 0, (n_r - 1) * r_step  # radius in meters
    # phi_min, phi_max = 0, np.deg2rad(359)
    # polar_surface = in_data["surfheight"]

    latlon_pic = in_data["height"].T[::-1, :]
    # plt.imshow(latlon_pic)
    # plt.show()

    lat_lon_interpolator = LatLonInterpolator(lat_min, lat_max, lon_min, lon_max, latlon_pic)

    x_min = -120e3
    x_max = 120e3
    n_x = 1201

    y_min = -100e3
    y_max = 100e3
    n_y = 1001

    # cart_pic = lat_lon_interpolator.generate_cartesian_pic(x_min, x_max, n_x, y_min, y_max, n_y)

    polar_pic = lat_lon_interpolator.generate_polar_pic(lat_stand, lon_stand, 0, 100e3, 1501, 0, 2 * np.pi, 361, False)
    plt.imshow(polar_pic)
    plt.show()


