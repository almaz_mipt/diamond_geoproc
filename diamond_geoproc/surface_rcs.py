from enum import IntEnum, unique
import logging
import math

import numpy as np
from scipy.stats import weibull_min

from diamond_geoproc.dafe_surface import dafe_reflectivity

@unique
class SurfaceType(IntEnum):
    """
    Типы поверхностей, поддерживаемых функциями расчёта удельной отражающей способности от угла падения.
    """
    UNKNOWN = -1
    SEA = 0
    RURAL_LOW_RELIEF = 1
    FOREST_LOW_RELIEF = 2
    FARMLAND_LOW_RELIEF = 3
    DESERT_LOW_RELIEF = 4
    RURAL_HIGH_RELIEF = 5
    FOREST_HIGH_RELIEF = 6
    MOUNTAINS_HIGH_RELIEF = 7
    GENERAL_URBAN_RELIEF = 8


@unique
class DafeSurfaceType(IntEnum):
    """
    Типы поверхностей, поддерживаемых функцией расчёта удельной
        отражающей способности от угла падения dafe_reflectivity
    """
    WATER = 0                                       # Водоемы
    RIVERS_STREAMS_CANALS = 1                       # Реки,ручьи,каналы
    WETLAND = 2                                     # Заболоченная местность
    GLACIERS_FIRN_FIELDS = 3                        # Ледники, фирновые поля
    DANGEROUS_LANDSCAPE = 4                         # Опасный ландшафт (Скалы,гряды,вулканы)
    RUGGED_TERRAIN = 5                              # Пересеченная местность
    SANDY_GROUND = 6                                # Песчаный грунт
    ROCKY_GROUND = 7                                # Каменистый грунт
    URBAN_SETTLEMENTS = 8                           # Населенные пункты городского типа
    RURAL_SETTLEMENTS = 9                           # Населенные пункты сельского типа
    INDUSTRIAL_FACILITIES = 10                      # Промышленные объекты
    RAILWAY_NETWORK = 11                            # Железнодорожная сеть
    ROAD_NETWORK = 12                               # Дорожная сеть
    FENCING = 13                                    # Ограждения
    SOURCES_OF_ELECTROMAGNETIC_RADIATION = 14       # Источники электромагнитного излучения
    ENVIRONMENTAL_HAZARD = 15                       # Экологическая опасность
    WOODY_VEGETATION = 16                           # Древесная растительность
    WINDBREAK = 17                                  # Буреломы
    SHRUB_VEGETATION = 18                           # Кустарниковая растительность


@unique
class LandReflType(IntEnum):
    """
    Способы расчёта отражающей способности земли
    """
    MODEL_1_RAND = 1  # расчёт удельной ЭПР с пом. land_clutter_reflectivity1, с рандомизацией
    MODEL_2_RAND = 2  # расчёт удельной ЭПР с пом. land_clutter_reflectivity2, с рандомизацией
    MODEL_1_NORAND = 3  # расчёт удельной ЭПР с пом. land_clutter_reflectivity1, без рандомизации
    MODEL_2_NORAND = 4  # расчёт удельной ЭПР с пом. land_clutter_reflectivity2, без рандомизации
    MODEL_3_NORAND = 5  # расчет с использованием dafe_reflectivity


def surface_rcs_per_unit(az_mesh, r_mesh, surftype, visibilities, slope_angles,
                         az_width, r_width, r_min, freq, cluttermodel,
                         h_above_sea, seastate=4):
    """
    Args:
        az_mesh: двумерный массив азимутов размерности [n_az, n_ranges]
        r_mesh: двумерный массив дальностей до фацетов размерности [n_az, n_ranges]
        surftype: двумерный массив типов поверхности фацетов размерности [n_az, n_ranges]
        visibilities: маска видимостей фацетов размерности [n_az, n_ranges]
        slope_angles: маска углов падений  # FIXME!!!: сейчас передаётся не то, а c какого-то дьявола углы возвышения
        az_width: размер фацета по азимуту
        r_width: размер фацета по дальности
        r_min:
        freq: несущая частота в Гц
        cluttermodel:
        h_above_sea: высота стояния антенны РЛС над уровнем моря
        seastate (float):

    Returns:
        numpy.ndarray(dtype=np.float64)
    """

    speed_of_light = 3.e8
    wavelength = speed_of_light / freq

    rcs = np.zeros_like(az_mesh, dtype=np.float64)
    rcs_aux = np.zeros(shape=(az_mesh.shape[1],), dtype=np.float64)

    n_az, n_r = az_mesh.shape

    if cluttermodel == LandReflType.MODEL_3_NORAND:
        resolution_areas = r_mesh * az_width * r_width
        # Ditch negative slope_angles values and
        # constrain incidence angles within 0...pi/2
        incidence_angles = np.abs(np.pi/2 - np.clip(slope_angles, 1e-10, None))
        rcs = dafe_reflectivity(visibilities, surftype,
                                incidence_angles, resolution_areas)
        return rcs

    for i in range(n_az):
        logging.debug("RCS CALC %i / %i " % (i, n_az))

        for j in range(n_r):
            r = r_mesh[i, j]
            if r < r_min or not visibilities[i, j]:  # invisible point
                rcs_aux[j] = 0.
            else:
                depressionangle = - slope_angles[i, j]
                resolutionarea = r * az_width * r_width
                surface = surftype[i, j]

                sigmadB = -100.
                aw = 1
                if surface == SurfaceType.SEA:  # Sea
                    # seastate = 4
                    psi = grazing_angle(r, h_above_sea)
                    if psi > 0.:
                        [sigmadB, aw] = sea_clutter_reflectivity(seastate, psi, wavelength)
                else:
                    if cluttermodel == LandReflType.MODEL_1_RAND or cluttermodel == LandReflType.MODEL_1_NORAND:
                        [sigmadB, aw] = land_clutter_reflectivity1(surface, depressionangle, resolutionarea)

                    if cluttermodel == LandReflType.MODEL_2_RAND or cluttermodel == LandReflType.MODEL_2_NORAND:
                        [sigmadB, aw] = land_clutter_reflectivity2(surface, visibilities[i, j], resolutionarea)  # FIXME

                sigma0 = resolutionarea * 10. ** (sigmadB / 10.)  # average RCS expected

                if cluttermodel == LandReflType.MODEL_1_RAND or cluttermodel == LandReflType.MODEL_2_RAND:
                    # generation random Weibull distributed RCS
                    c = 1. / aw
                    b = sigma0 / math.gamma(1. + aw)
                    a = (1. / b) ** c
                    # sigma = (sigma0 / gamma(1 + aw)) * (randn ^ 2 / 2 + randn ^ 2 / 2) ^ aw;
                    #sigma = np.random.weibull(a, c)

                    # FIXME: Обязательно потыкать Сергея Афанасьева, чтобы он посмотрел этот кусочек ещё раз:
                    # похоже, здесь надо брать случайную величину, заданную по распределению Вейбула,
                    # а не брать плотность вероятности от распределения Вейбула!!!
                    sigma = weibull_min.pdf(a, c)
                    #sigma = weibull_min.pdf(0.99, 1.79)
                    rcs_aux[j] = sigma
                else:
                    # generation const RCS equal average value
                    rcs_aux[j] = sigma0

        # small smoothing along range_ gates
        a = 0.1
        c = 0.1
        b = 1. - a - c
        for j in range(1, n_r-2):
            rcs[i, j] = a * rcs_aux[j - 1] + b * rcs_aux[j] + c * rcs_aux[j + 1]
        rcs[i, 0] = 0.7 * rcs_aux[0]
        rcs[i, n_r-1] = 0.7 * rcs_aux[n_r-1]

    # plt.imshow(rcs)
    # plt.show()

    return rcs


def grazing_angle(r, h_above_sea):
    """
    Расчёт угла падения луча на морскую поверхность.

    Args:
        r(float): дальность до фацета
        h_above_sea(float): высота центра антенны над уровнем моря
    """
    Reff = 1.333 * 6371000.
    psi = math.acos((r ** 2 - 2 * Reff * h_above_sea - h_above_sea ** 2) / (2 * r * Reff)) - math.pi / 2
    return psi


def sea_clutter_reflectivity(seastate, grazingangle, wavelength):
    """
    Отражающая способность морской поверхности

    Args:
        seastate:
        grazingangle:
        wavelength:
    Returns:

    """
    gamma_dB = 6. * seastate - 10. * math.log10(wavelength) - 58.
    gamma = 10. ** (gamma_dB/10)
    sigma = gamma * math.sin(grazingangle)
    sigmadB = 10. * math.log10(sigma)
    aw = 1
    return sigmadB, aw


def land_clutter_reflectivity1(terraintype, depressionangle, resolutionarea):

    ascale = (math.log10(resolutionarea) - 3.) / 3.
    angle = depressionangle * 180 / math.pi

    if terraintype != 6 and terraintype != 7 and angle < 0:
        if angle >= -0.25:
            sigmadB = -31; aw = (1-ascale) * 3.4 + ascale * 2.0
            return sigmadB, aw
        if angle >= -0.75:
            sigmadB = -27; aw = (1-ascale) * 3.3 + ascale * 1.9
            return sigmadB, aw
        if angle < -0.75:
            sigmadB = -26; aw = (1-ascale) * 2.3 + ascale * 1.7
            return sigmadB, aw

    if terraintype == SurfaceType.RURAL_LOW_RELIEF:  # general rural low relief
        if angle > 4.0:
            sigmadB = -25; aw = (1-ascale) * 2.6 + ascale * 1.5
            return sigmadB, aw
        if angle > 1.5:
            sigmadB = -27; aw = (1-ascale) * 2.7 + ascale * 1.6
            return sigmadB, aw
        if angle > 0.75:
            sigmadB = -30; aw = (1-ascale) * 3.0 + ascale * 1.8
            return sigmadB, aw
        if angle > 0.25:
            sigmadB = -32; aw = (1-ascale) * 3.5 + ascale * 2.2
            return sigmadB, aw
        if angle >= 0.0:
            sigmadB = -33; aw = (1-ascale) * 3.8 + ascale * 2.5
            return sigmadB, aw

    if terraintype == SurfaceType.FOREST_LOW_RELIEF:  # forest low relief
        if angle > 1.0:
            sigmadB = -26; aw = (1-ascale) * 2.0 + ascale * 1.3
            return sigmadB, aw
        if angle > 0.3:
            sigmadB = -30; aw = (1-ascale) * 2.7 + ascale * 1.6
            return sigmadB, aw
        if angle >= 0.0:
            sigmadB = -37; aw = (1-ascale) * 3.2 + ascale * 1.8
            return sigmadB, aw

    if terraintype == SurfaceType.FARMLAND_LOW_RELIEF:  # farmland low relief
        if angle > 1.5:
            sigmadB = -30; aw = (1-ascale) * 3.3 + ascale * 2.4
            return sigmadB, aw
        if angle > 0.75:
            sigmadB = -30; aw = (1-ascale) * 3.3 + ascale * 2.4
            return sigmadB, aw
        if angle > 0.4:
            sigmadB = -30; aw = (1-ascale) * 4.0 + ascale * 2.6
            return sigmadB, aw
        if angle >= 0.0:
            sigmadB = -30; aw = (1-ascale) * 5.4 + ascale * 2.8
            return sigmadB, aw

    if terraintype == SurfaceType.DESERT_LOW_RELIEF:  # Desert low relief
        if angle > 0.75:
            sigmadB = -26; aw = (1-ascale) * 2.0 + ascale * 1.3
            return sigmadB, aw
        if angle > 0.25:
            sigmadB = -36; aw = (1-ascale) * 2.7 + ascale * 1.6
            return sigmadB, aw
        if angle >= 0.0:
            sigmadB = -42; aw = (1-ascale) * 3.8 + ascale * 1.8
            return sigmadB, aw

    if terraintype == SurfaceType.RURAL_HIGH_RELIEF:  # general rural high relief
        if angle > 6:
            sigmadB = -19; aw = (1-ascale) * 1.5 + ascale * 1.1
            return sigmadB, aw
        if angle > 4:
            sigmadB = -21; aw = (1-ascale) * 1.6 + ascale * 1.2
            return sigmadB, aw
        if angle > 2:
            sigmadB = -24; aw = (1-ascale) * 1.8 + ascale * 1.3
            return sigmadB, aw
        if angle >= 0:
            sigmadB = -27; aw = (1-ascale) * 2.2 + ascale * 1.4
            return sigmadB, aw

    if terraintype == SurfaceType.FOREST_HIGH_RELIEF:  # forest high relief
        sigmadB = -22
        aw = (1 - ascale) * 1.8 + ascale * 1.3
        return sigmadB, aw

    if terraintype == SurfaceType.MOUNTAINS_HIGH_RELIEF:  # mountains high relief
        sigmadB = -20
        aw = (1 - ascale) * 2.8 + ascale * 1.6
        return sigmadB, aw

    if terraintype == SurfaceType.GENERAL_URBAN_RELIEF:  # General urban
        if angle > 0.75:
            sigmadB = -20; aw = (1-ascale) * 3.0 + ascale * 2.0
            return sigmadB, aw
        if angle > 0.25:
            sigmadB = -20; aw = (1-ascale) * 3.7 + ascale * 2.4
            return sigmadB, aw
        if angle >= 0.0:
            sigmadB = -20; aw = (1-ascale) * 4.3 + ascale * 2.8
            return sigmadB, aw


def land_clutter_reflectivity2(terraintype, grazingangle, resolutionarea):

    # SurfaceType coding in TerrainClutterModel:
    # 1 - Rural low relief
    # 2 - Forest low relief
    # 3 - Farmland low relief
    # 4 - Desert, marsh low relief
    # 5 - Rural high relief
    # 6 - Forest high relief
    # 7 - Mountains
    # 8 - Urban
    # 9 - Auto (as in radar map)

    sigma0_dB = -100
    if terraintype == SurfaceType.RURAL_LOW_RELIEF:  # general rural low relief
        sigma0_dB = -32
    if terraintype == SurfaceType.FOREST_LOW_RELIEF:  # forest low relief
        sigma0_dB = -28
    if terraintype == SurfaceType.FARMLAND_LOW_RELIEF:  # farmland low relief
        sigma0_dB = -28
    if terraintype == SurfaceType.DESERT_LOW_RELIEF:  # Desert low relief
        sigma0_dB = -32
    if terraintype == SurfaceType.RURAL_HIGH_RELIEF:  # general rural high relief
        sigma0_dB = -28
    if terraintype == SurfaceType.FOREST_HIGH_RELIEF:  # forest high relief
        sigma0_dB = -24
    if terraintype == SurfaceType.MOUNTAINS_HIGH_RELIEF:  # mountains high relief
        sigma0_dB = -28
    if terraintype == SurfaceType.GENERAL_URBAN_RELIEF:  # General urban
        sigma0_dB = -20

    if grazingangle < 0.05:
        sigmadB = sigma0_dB
    else:
        sigmadB = sigma0_dB + 10. * math.log10(grazingangle/0.05)

    if grazingangle > 0.1:
        aw = 1.
    else:
        if grazingangle < 0.02:
            aw = 4.
        else:
            aw = 4. + 3. * (0.02 - grazingangle) / 0.08

    return sigmadB, aw
