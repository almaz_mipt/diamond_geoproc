import sys
import os

from diamond_geoproc.map_sheet_processing import create_coverage_dataframe


def main(path_to_geotiffs):
    df = create_coverage_dataframe(path_to_geotiffs)
    df.to_csv(os.path.join(path_to_geotiffs, 'coverage.csv'), sep="\t", header=True,
              index=False)


main(sys.argv[1])