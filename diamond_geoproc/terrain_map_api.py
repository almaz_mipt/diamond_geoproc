import rasterio.transform
import rasterio.plot
import rasterio.shutil
import tempfile

import numpy as np

from diamond_geoproc.map_sheet_processing import *
from diamond_geoproc.background_interpolation import LatLonInterpolator
from diamond_geoproc.surface_rcs import DafeSurfaceType, SurfaceType


class TerrainMap:
    """
    Класс для работы с картами свойств местности
    """

    EARTH_RADIUS = 6371000.  # Mean Earth radius in m
    EFF_EARTH_RADIUS = 1.33 * EARTH_RADIUS  # Effective Earth radius in m
    # FIXME: поставить 4/3 Rз, это имеет значение

    def __init__(self, lon, lat, dataset, nodata_as_sea=False,
                 dedicated_sea_value=-200,
                 default_surface_type=SurfaceType.FOREST_HIGH_RELIEF):
        """
        Args:
            dataset (rasterio.DatasetReader): открытый на чтение
                георастр с высотами рельефа над уровнем моря
            is_sea_threshold(float): то, что не nodata, но меньше
                этого порога, считать морем!
        """

        self._lon_stand = float(lon)
        self._lat_stand = float(lat)

        if dataset:
            self.dataset_available = True
            # READING RASTER DATA
            assert isinstance(dataset, rasterio.DatasetReader)
            self._src_raster = dataset.read(1)[::-1, :]

            self._raster_lat_min = dataset.bounds.bottom
            self._raster_lon_min = dataset.bounds.left

            self._lat_step, self._lon_step = dataset.res
            self._n_raster_lats, self._n_raster_lons = self._src_raster.shape

            self._raster_lat_max = self._raster_lat_min + self._lat_step * (self._n_raster_lats - 1)
            self._raster_lon_max = self._raster_lon_min + self._lon_step * (self._n_raster_lons - 1)

            # Если в георастре установлено это значение, то это значит, что тут
            # нет валидных высот (например, в SRTM для моря)
            self._nodata = dataset.nodata
            # Воспринимать ли значение nodata как море
            self._nodata_as_sea = bool(nodata_as_sea)

            no_data_mask = (self._src_raster == self._nodata)
            is_sea_mask = np.zeros_like(self._src_raster, dtype=np.bool_)
            if self._nodata_as_sea:
                is_sea_mask[:] = (self._src_raster == self._nodata)
            if dedicated_sea_value is not None:
                is_sea_mask[:] = is_sea_mask[:] | (self._src_raster <= dedicated_sea_value)

            # Корректировка карты высот: море должно быть на 0 метрах
            # над уровнем моря
            raster_data_with_sealevel = self._src_raster.copy()
            raster_data_with_sealevel[np.where(is_sea_mask)] = 0

            # Интерполятор, возвращающий высоту поверхности над уровнем моря
            self._land_height_interpolator = LatLonInterpolator(
                self._raster_lat_min, self._raster_lat_max, self._raster_lon_min,
                self._raster_lon_max, raster_data_with_sealevel,
                take_nearest=False, fill_value=0)

            # Формирование карты типа рельефа по данным из карты высот
            surface_type_mesh = np.zeros_like(self._src_raster, dtype=np.int32)
            surface_type_mesh[:, :] = default_surface_type
            surface_type_mesh[np.where(no_data_mask)] = DafeSurfaceType.WATER
            surface_type_mesh[np.where(is_sea_mask)] = DafeSurfaceType.WATER

            # Интерполятор, возвращающий тип поверхности, полученный
            # из карты высот в заданных точках
            self.surf_type_interpolator = LatLonInterpolator(
                self._raster_lat_min, self._raster_lat_max, self._raster_lon_min,
                self._raster_lon_max, surface_type_mesh,
                take_nearest=True, fill_value=0)

        else:
            self.dataset_available = False
            # Interpolator, which will return zeros everywhere
            self._land_height_interpolator = LatLonInterpolator(
                0, 90, 0, 90, np.asarray([[0, 0], [0, 0]]),
                take_nearest=False, fill_value=0)
            self.surf_type_interpolator = LatLonInterpolator(
                0, 90, 0, 90, np.asarray([[0, 0], [0, 0]]),
                take_nearest=False, fill_value=0)


class TerrainMapLoader:
    def __init__(self, map_dir_path, surface_type_fallback_value=False,
                 relief_fallback_to_zero=False):
        self._map_dir_path = map_dir_path
        self._coverage_info_path = os.path.join(map_dir_path, "coverage.csv")
        df = load_coverage_dataframe(self._coverage_info_path)
        self._idx = create_coverage_index(df)

        self._temp_dir = tempfile.TemporaryDirectory()
        self.surface_type_fallback_value = surface_type_fallback_value
        self.relief_fallback_to_zero = relief_fallback_to_zero

    def create_terrain_map(self, lon_stand, lat_stand, r_max):
        tifs_to_load = select_geotiff_names(self._idx, lon_stand, lat_stand, r_max)
        if not tifs_to_load:
            return TerrainMap(
                lon_stand,
                lat_stand,
                None,
                default_surface_type=self.surface_type_fallback_value,
            )

        abs_fnames = []
        for file_name, zip_name in tifs_to_load:
            if isinstance(zip_name, str) or not np.isnan(zip_name):
                abs_fnames.append("/vsizip/%s/%s" % (os.path.join(self._map_dir_path, file_name), zip_name))
            else:
                abs_fnames.append("%s" % (os.path.join(self._map_dir_path, file_name)))

        virtual_raster_path = os.path.join(self._temp_dir.name, "temp_ter.vrt")
        gdal.BuildVRT(virtual_raster_path, abs_fnames)
        return TerrainMap(
            lon_stand,
            lat_stand,
            rasterio.open(virtual_raster_path),
            default_surface_type=self.surface_type_fallback_value,
        )
