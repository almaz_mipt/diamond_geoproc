
import os, sys
import requests



if __name__ == "__main__":
    dump_folder_name = "/media/kitekat/DUMP2/DOWN/srtm_dumps"
    n_rows, n_cols = 24, 72
    for col in range(n_cols):
        for row in range(n_rows):
            print(row, col)
            url = "http://srtm.csi.cgiar.org/SRT-ZIP/SRTM_V41/SRTM_Data_GeoTiff/srtm_%.2i_%.2i.zip" % (col + 1, row + 1)
            path_to_dump = os.path.join(dump_folder_name, "srtm_%.2i_%.2i.zip" % (col + 1, row + 1))
            r = requests.get(url, allow_redirects=True)
            open(path_to_dump, 'wb').write(r.content)

