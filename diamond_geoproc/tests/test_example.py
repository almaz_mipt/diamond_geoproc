import pytest

from pathlib import Path

import numpy as np

from diamond_geoproc.background_generation import PolarMeshData, BgType,\
    SurfaceTypeDataNotAvailableException
from diamond_geoproc.earth_polar_map_api import EarthPolarMap
from diamond_geoproc.surface_map_api import SurfaceMapLoader
from diamond_geoproc.terrain_map_api import TerrainMapLoader, TerrainMap


@pytest.fixture
def srtm_dumps_dir():
    path = Path(__file__).parents[3] / "srtm_dumps"
    return str(path.absolute())


@pytest.fixture
def surface_type_dumps_dir():
    path = Path(__file__).parents[3] / "surface_type_dumps"
    return str(path.absolute())


def test_polar_map_generation(srtm_dumps_dir, surface_type_dumps_dir):
    # Координаты основания РЛС
    lon, lat, h_stand = 22.041944444444447, 37.058055555555555, 0
    
    # Высота антенны над основанием РЛС
    h_ant = 8.
    
    # Параметры mesh-сети
    r_min, r_max, n_r = 89.98875140607424, 80000.0, 889
    az_min, az_max, n_az = 0.0, 6.265732014659643, 360

    # Задаём размер фацета (обычно выбирают исходя из ширины ДНА)
    facet_az_width, facet_r_width = 0.03490658503988659, 90
    freq = 10000000000.0

    # Задаём параметры сетки в полярных координатах (азимут-дальность)
    mesh_data = PolarMeshData(r_min=r_min, r_max=r_max, n_r=n_r,
                              az_min=az_min, az_max=az_max, n_az=n_az)

    # Создаём сам TerrainMap-объект (виртуальный растр, сделанный из геотифок,
    # находящихся в заданном радиусе от точки стояния РЛС)
    map_loader = TerrainMapLoader(srtm_dumps_dir)
    termap = map_loader.create_terrain_map(lon, lat, r_max)
    surface_map_loader = SurfaceMapLoader(surface_type_dumps_dir)
    surfmap = surface_map_loader.create_surface_map(lon, lat, r_max)
    earth_polar_map = EarthPolarMap(lon, lat, termap, surfmap)

    # И собственно рассчитываем полярные карты (рассчитываем
    # в специальном режиме совместимости со старым XRADARом,
    # баг-в-баг)
    with pytest.raises(SurfaceTypeDataNotAvailableException):
        polar_maps, mesh_data, h_above_sea = earth_polar_map.create_polar_maps(
            h_stand, h_ant, mesh_data, facet_az_width, facet_r_width, freq,
            compatibility_mode=True)
