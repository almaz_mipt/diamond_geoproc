from numba import boolean, float64, guvectorize, int64, njit
import numpy as np
from numpy import cos, pi, sin, sqrt


@njit
def calculate_RCS_kustarnikovaya_rastitelnost(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * np.pi * f / c
    
    A = 0.578063
    Na = 0.619765
    eps_r = -0.996995
    eps_i = 0.424945
    
    alpha_v = (3 / 8) * Na * A * eps_i * (1 + 12 / (1 + eps_r)**2 + (1 - 4 / (1 + eps_r)**2) * cos((pi / 2 - thetta))**2) / sin(pi / 2 - thetta)
    sigma_vv = (Na * A**2 * k**2 / (4 * pi)) * ((((eps_r - 1)**2 + eps_i**2) * (3 + 16 / (1 + eps_r) + 96 / (1 + eps_r)**2 + cos((pi / 2 - thetta))**2 * ( 12 + 8 / (1 + eps_r) - 64 / (1 + eps_r)**2)) / 35) / ((3 / 5) * alpha_v**2 + (4 / 5) * (1 + 2 * sin((pi / 2 - thetta))**2)))
    return sigma_vv


@njit
def calculate_RCS_drevesnaya_rastitelnost(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    A = -0.01399
    Na = 0.61976
    eps_r = -0.99704
    eps_i = 0.42494
    
    alpha_v = (3 / 8) * Na * A * eps_i * (1 + 12 / (1 + eps_r)**2 + (1 - 4 / (1 + eps_r)**2) * cos((pi / 2 - thetta))**2) / sin(pi / 2 - thetta)
    sigma_vv = (Na * A**2 * k**2 / (4 * pi)) * ((((eps_r - 1)**2 + eps_i**2) * (3 + 16 / (1 + eps_r) + 96 / (1 + eps_r)**2 + cos((pi / 2 - thetta))**2 * ( 12 + 8 / (1 + eps_r) - 64 / (1 + eps_r)**2)) / 35) / ((3 / 5) * alpha_v**2 + (4 / 5) * (1 + 2 * sin((pi / 2 - thetta))**2)))
    return sigma_vv


@njit
def istochniki_el_izl(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    eps_r = 0.645487
    eps_i = 1.06197
    mu_r = 0.127043
    mu_i = -0.638949
    betta = 0.781635
    
    phi_i = pi
    phi_s = 0
    
    qx = k*(sin(thetta)*cos(phi_s) - sin(thetta)*cos(phi_i))
    qy = k*(sin(thetta)*sin(phi_s) - sin(thetta)*sin(phi_i))
    qz = 2*k*cos(thetta)
    q = sqrt(qx**2 + qy**2 + qz**2)
    
    R_par = ((eps_r + 1j*eps_i) * cos(thetta) - sqrt((eps_r + 1j*eps_i)*(mu_r + 1j*mu_i) - sin(thetta)))/ \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt((eps_r + 1j*eps_i)*(mu_r + 1j*mu_i) - sin(thetta)))
    R_per = ((mu_r + 1j*mu_i) * cos(thetta) - sqrt((eps_r + 1j*eps_i)*(mu_r + 1j*mu_i) - sin(thetta)))/ \
    ((mu_r + 1j*mu_i) * cos(thetta) + sqrt((eps_r + 1j*eps_i)*(mu_r + 1j*mu_i) - sin(thetta)))
    
    Uhh = R_par * cos(thetta)
    Uvv = R_per * cos(thetta)
    
    sigma_vv = k**2 * q**2 * abs(Uvv**2) * np.exp(-(qx**2 + qy**2)/(2*qz**2 * betta**2)) / (2 * qz**4 * betta**2)
    return sigma_vv.real


@njit
def prom_objects(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    eps_r = 0.620257
    eps_i = 0.118738
    betta = 0.34366
    
    phi_i = pi
    phi_s = 0
    
    qx = k*(sin(thetta)*cos(phi_s) - sin(thetta)*cos(phi_i))
    qy = k*(sin(thetta)*sin(phi_s) - sin(thetta)*sin(phi_i))
    qz = 2*k*cos(thetta)
    q = sqrt(qx**2 + qy**2 + qz**2)
    
    R_par = ((eps_r + 1j*eps_i) * cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    R_per = (cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    (cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    
    Uhh = R_par * cos(thetta)
    Uvv = R_per * cos(thetta)
    
    sigma_vv = k**2 * q**2 * abs(Uvv**2) * np.exp(-(qx**2 + qy**2)/(2*qz**2 * betta**2)) / (2 * qz**4 * betta**2)
    return sigma_vv.real


@njit
def selsk_type(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    eps_r = 0.913794
    eps_i = 1.87948
    betta = 0.467014
    
    phi_i = pi
    phi_s = 0
    
    qx = k*(sin(thetta)*cos(phi_s) - sin(thetta)*cos(phi_i))
    qy = k*(sin(thetta)*sin(phi_s) - sin(thetta)*sin(phi_i))
    qz = 2*k*cos(thetta)
    q = sqrt(qx**2 + qy**2 + qz**2)
    
    R_par = ((eps_r + 1j*eps_i) * cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    R_per = (cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    (cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    
    Uhh = R_par * cos(thetta)
    Uvv = R_per * cos(thetta)
    
    sigma_vv = k**2 * q**2 * abs(Uvv**2) * np.exp(-(qx**2 + qy**2)/(2*qz**2 * betta**2)) / (2 * qz**4 * betta**2)
    return sigma_vv.real


@njit
def gor_type(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    eps_r = -0.412955
    eps_i = 1.66729
    betta = 0.531441
    
    phi_i = pi
    phi_s = 0
    
    qx = k*(sin(thetta)*cos(phi_s) - sin(thetta)*cos(phi_i))
    qy = k*(sin(thetta)*sin(phi_s) - sin(thetta)*sin(phi_i))
    qz = 2*k*cos(thetta)
    q = sqrt(qx**2 + qy**2 + qz**2)
    
    R_par = ((eps_r + 1j*eps_i) * cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    R_per = (cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    (cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    
    Uhh = R_par * cos(thetta)
    Uvv = R_per * cos(thetta)
    
    sigma_vv = k**2 * q**2 * abs(Uvv**2) * np.exp(-(qx**2 + qy**2)/(2*qz**2 * betta**2)) / (2 * qz**4 * betta**2)
    return sigma_vv.real


@njit
def skaly_vulkany(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    eps_r = 1.1367726988480462
    eps_i = 2.3042357537151816
    betta = 0.4481226882541811
    
    phi_i = pi
    phi_s = 0
    
    qx = k*(sin(thetta)*cos(phi_s) - sin(thetta)*cos(phi_i))
    qy = k*(sin(thetta)*sin(phi_s) - sin(thetta)*sin(phi_i))
    qz = 2*k*cos(thetta)
    q = sqrt(qx**2 + qy**2 + qz**2)
    
    R_par = ((eps_r + 1j*eps_i) * cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    R_per = (cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    (cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    
    Uhh = R_par * cos(thetta)
    Uvv = R_per * cos(thetta)
    
    sigma_vv = k**2 * q**2 * abs(Uvv**2) * np.exp(-(qx**2 + qy**2)/(2*qz**2 * betta**2)) / (2 * qz**4 * betta**2)
    return sigma_vv.real


@njit
def doroszhn_set(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    h = 0.029321
    l = 1.19639
    eps_r = 2.80188
    eps_i = 1.54951
    
    alpha_hh = ((eps_r + 1j*eps_i) - 1)/(cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    alpha_vv = ((eps_r + 1j*eps_i - 1) * ((eps_r + 1j*eps_i - 1) * sin(thetta)**2 + eps_r + 1j*eps_i)) / \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    I_exp = 2 * pi * l**2 / (1 + (2 * k * sin(thetta) * l)**2)**1.5
    sigma_vv = (4 / pi) * k**4 * h**2 * abs((alpha_vv * cos(thetta)**2)**2) * I_exp
    sigma_hh = (4 / pi) * k**4 * h**2 * abs((alpha_hh * cos(thetta)**2)**2) * I_exp
    return sigma_vv.real


@njit
def kamen_grunt(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    h = -0.0531603
    l = 1.79245
    eps_r = 2.10037
    eps_i = 1.28156
    
    alpha_hh = ((eps_r + 1j*eps_i) - 1)/(cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    alpha_vv = ((eps_r + 1j*eps_i - 1) * ((eps_r + 1j*eps_i - 1) * sin(thetta)**2 + eps_r + 1j*eps_i)) / \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    I_exp = 2 * pi * l**2 / (1 + (2 * k * sin(thetta) * l)**2)**1.5
    sigma_vv = (4 / pi) * k**4 * h**2 * abs((alpha_vv * cos(thetta)**2)**2) * I_exp
    sigma_hh = (4 / pi) * k**4 * h**2 * abs((alpha_hh * cos(thetta)**2)**2) * I_exp
    return sigma_vv.real


@njit
def pesch_grunt(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    h = 0.081628
    l = 2.55819
    eps_r = 0.534611
    eps_i = -0.104701
    
    alpha_hh = ((eps_r + 1j*eps_i) - 1)/(cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    alpha_vv = ((eps_r + 1j*eps_i - 1) * ((eps_r + 1j*eps_i - 1) * sin(thetta)**2 + eps_r + 1j*eps_i)) / \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    I_exp = 2 * pi * l**2 / (1 + (2 * k * sin(thetta) * l)**2)**1.5
    sigma_vv = (4 / pi) * k**4 * h**2 * abs((alpha_vv * cos(thetta)**2)**2) * I_exp
    sigma_hh = (4 / pi) * k**4 * h**2 * abs((alpha_hh * cos(thetta)**2)**2) * I_exp
    return sigma_vv.real


@njit
def zhelez_dorog_set(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    h = 0.0385368
    l = 2.58691
    eps_r = 1.23308
    eps_i = -2.72859
    
    alpha_hh = ((eps_r + 1j*eps_i) - 1)/(cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    alpha_vv = ((eps_r + 1j*eps_i - 1) * ((eps_r + 1j*eps_i - 1) * sin(thetta)**2 + eps_r + 1j*eps_i)) / \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    I_exp = 2 * pi * l**2 / (1 + (2 * k * sin(thetta) * l)**2)**1.5
    sigma_vv = (4 / pi) * k**4 * h**2 * abs((alpha_vv * cos(thetta)**2)**2) * I_exp
    sigma_hh = (4 / pi) * k**4 * h**2 * abs((alpha_hh * cos(thetta)**2)**2) * I_exp
    return sigma_vv.real


@njit
def peres_mest(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    h = -0.160726
    l = 1.13694
    eps_r = 0.80361
    eps_i = 0.0400341
    
    alpha_hh = ((eps_r + 1j*eps_i) - 1)/(cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    alpha_vv = ((eps_r + 1j*eps_i - 1) * ((eps_r + 1j*eps_i - 1) * sin(thetta)**2 + eps_r + 1j*eps_i)) / \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    I_exp = 2 * pi * l**2 / (1 + (2 * k * sin(thetta) * l)**2)**1.5
    sigma_vv = (4 / pi) * k**4 * h**2 * abs((alpha_vv * cos(thetta)**2)**2) * I_exp
    sigma_hh = (4 / pi) * k**4 * h**2 * abs((alpha_hh * cos(thetta)**2)**2) * I_exp
    return sigma_vv.real


@njit
def led_i_fir_polya(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    h = -0.0224317
    l = 0.655746
    eps_r = 4.33661
    eps_i = -0.310987
    
    alpha_hh = ((eps_r + 1j*eps_i) - 1)/(cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    alpha_vv = ((eps_r + 1j*eps_i - 1) * ((eps_r + 1j*eps_i - 1) * sin(thetta)**2 + eps_r + 1j*eps_i)) / \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    I_exp = 2 * pi * l**2 / (1 + (2 * k * sin(thetta) * l)**2)**1.5
    sigma_vv = (4 / pi) * k**4 * h**2 * abs((alpha_vv * cos(thetta)**2)**2) * I_exp
    sigma_hh = (4 / pi) * k**4 * h**2 * abs((alpha_hh * cos(thetta)**2)**2) * I_exp
    return sigma_vv.real


@njit
def zabol_mest(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    h = -0.0544091
    l = 2.29837
    eps_r = 1.88793
    eps_i = -0.905038
    
    alpha_hh = ((eps_r + 1j*eps_i) - 1)/(cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    alpha_vv = ((eps_r + 1j*eps_i - 1) * ((eps_r + 1j*eps_i - 1) * sin(thetta)**2 + eps_r + 1j*eps_i)) / \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    I_exp = 2 * pi * l**2 / (1 + (2 * k * sin(thetta) * l)**2)**1.5
    sigma_vv = (4 / pi) * k**4 * h**2 * abs((alpha_vv * cos(thetta)**2)**2) * I_exp
    sigma_hh = (4 / pi) * k**4 * h**2 * abs((alpha_hh * cos(thetta)**2)**2) * I_exp
    return sigma_vv.real


@njit
def water(thetta):
    l = 0.01
    psi = thetta
    k = 2*pi/l
    epsilon = 75
    mu = 1
    U10 = 10
    c_p = 1.17*U10
    g = 9.81
    c = sqrt(g/k)
    Omega = U10/c_p
    k_p = g * Omega**2 / U10**2
    k_m = 378
    gamma = 1.7+6*np.log(Omega)
    a_0 = np.log(2)/4
    a_p = 4
    c_m = 0.23
    a_m = 0.014*0.4/c_m
    F_m = np.exp(-0.25*(k/k_m-1)**2)
    delta = np.tanh((a_0+a_p*(c/c_p)**2.5+a_m*(c_m/c)**2.5).real)
    sigma = 0.08*(1+4*Omega**-3)
    Gamma = np.exp((-(sqrt(k/k_p)-1)**2/2*sigma**2).real)
    J_p = gamma**Gamma
    
    F_p = np.exp((-5/4*(k_p/k)**2))*J_p*np.exp((-Omega/sqrt(10)*(sqrt(k/k_p-1))).real)

    k1 = sqrt(epsilon*mu - sin(psi)**2)
    a_h = (k1**2 - mu*sin(psi)**2)/((mu*cos(psi)+k1))**2*(mu-1)- \
            mu**2*(epsilon-1)/((mu*cos(psi)+k1))**2
    a_v = (k1**2 - epsilon*sin(psi)**2)/((epsilon*cos(psi)+k1))**2*(epsilon-1)- \
            epsilon**2*(mu-1)/((epsilon*cos(psi)+k1))**2
    B_l = 0.003*Omega**0.55*c_p/c*F_p
    B_h = 0.5*a_m*c_m/c*F_m
    S = k**-3*(B_l + B_h)*(1 + delta*cos(2*psi))
    sigma_hh = 8*k**4*(cos(psi)**2*a_h)**2*S
    sigma_vv = 8*k**4*(cos(psi)**2*a_v)**2*S
    return sigma_vv.real


@njit
def ograzhd(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    h = 0.0124705
    l = 1.01845
    eps_r = -1.1518
    eps_i = -1.19711
    
    alpha_hh = ((eps_r + 1j*eps_i) - 1)/(cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    alpha_vv = ((eps_r + 1j*eps_i - 1) * ((eps_r + 1j*eps_i - 1) * sin(thetta)**2 + eps_r + 1j*eps_i)) / \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt(eps_r + 1j*eps_i - sin(thetta)**2))**2
    I_exp = 2 * pi * l**2 / (1 + (2 * k * sin(thetta) * l)**2)**1.5
    sigma_vv = (4 / pi) * k**4 * h**2 * abs((alpha_vv * cos(thetta)**2)**2) * I_exp
    sigma_hh = (4 / pi) * k**4 * h**2 * abs((alpha_hh * cos(thetta)**2)**2) * I_exp
    return sigma_vv.real


@njit
def eko_opasn(thetta):
    f = 5.4 * 10**9
    c = 3 * 10**8
    k = 2 * pi * f / c
    
    eps_r = 0.618636
    eps_i = 1.54434
    betta = 0.428343
    
    phi_i = pi
    phi_s = 0
    
    qx = k*(sin(thetta)*cos(phi_s) - sin(thetta)*cos(phi_i))
    qy = k*(sin(thetta)*sin(phi_s) - sin(thetta)*sin(phi_i))
    qz = 2*k*cos(thetta)
    q = sqrt(qx**2 + qy**2 + qz**2)
    
    R_par = ((eps_r + 1j*eps_i) * cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    ((eps_r + 1j*eps_i) * cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    R_per = (cos(thetta) - sqrt((eps_r + 1j*eps_i) - sin(thetta)))/ \
    (cos(thetta) + sqrt((eps_r + 1j*eps_i) - sin(thetta)))
    
    Uhh = R_par * cos(thetta)
    Uvv = R_per * cos(thetta)
    
    sigma_vv = k**2 * q**2 * abs(Uvv**2) * np.exp(-(qx**2 + qy**2)/(2*qz**2 * betta**2)) / (2 * qz**4 * betta**2)
    return sigma_vv.real


@guvectorize([(boolean[:], int64[:], float64[:], float64[:], float64[:])],
             '(n),(n),(n),(n)->(n)')
def dafe_reflectivity(visibilities, dafe_terraintype, angle,
                      resolution_areas, rcs):
    for i in range(dafe_terraintype.shape[0]):
        if visibilities[i]:
            if dafe_terraintype[i] == 0 or dafe_terraintype[i] == 1:
                sigma = water(angle[i])
            elif dafe_terraintype[i] == 2:
                sigma = zabol_mest(angle[i])
            elif dafe_terraintype[i] == 3:
                sigma = led_i_fir_polya(angle[i])
            elif dafe_terraintype[i] == 4:
                sigma = skaly_vulkany(angle[i])
            elif dafe_terraintype[i] == 5:
                sigma = peres_mest(angle[i])
            elif dafe_terraintype[i] == 6:
                sigma = pesch_grunt(angle[i])
            elif dafe_terraintype[i] == 7:
                sigma = kamen_grunt(angle[i])
            elif dafe_terraintype[i] == 8:
                sigma = gor_type(angle[i])
            elif dafe_terraintype[i] == 9:
                sigma = selsk_type(angle[i])
            elif dafe_terraintype[i] == 10:
                sigma = prom_objects(angle[i])
            elif dafe_terraintype[i] == 11:
                sigma = zhelez_dorog_set(angle[i])
            elif dafe_terraintype[i] == 12:
                sigma = doroszhn_set(angle[i])
            elif dafe_terraintype[i] == 13:
                sigma = ograzhd(angle[i])
            elif dafe_terraintype[i] == 14:
                sigma = istochniki_el_izl(angle[i])
            elif dafe_terraintype[i] == 15:
                sigma = eko_opasn(angle[i])
            elif dafe_terraintype[i] == 16:
                sigma = calculate_RCS_drevesnaya_rastitelnost(angle[i])
            elif dafe_terraintype[i] == 18:
                sigma = calculate_RCS_kustarnikovaya_rastitelnost(angle[i])
            else:
                sigma = 0
            rcs[i] = resolution_areas[i] * sigma
        else:
            rcs[i] = 0


if __name__ == '__main__':
    print(dafe_reflectivity([True],
                            [0],
                            [pi/4],
                            [100]))
